<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['room_types'] = array('Single', 'Double', 'Quad', 'Deluxe', 'Suite', 'Apartment', 'Villa');
$config['gst'] = 18.0;
$config['hotel_address'] = 'Shibalaya Road Old Digha, Purba Medinipur, West Bengal, 721428';
$config['address1'] = 'Shibalaya Road Old Digha, Purba Medinipur';
$config['address2'] = 'West Bengal, 721428';
$config['landmark'] = 'Opposite Shiv Mandir';
$config['phone'] = '+919800064781 | +918584032767';
$config['map'] = 'https://goo.gl/2RdE6k';
$config['allowed_roles'] = ['admin','user'];