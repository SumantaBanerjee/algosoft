<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/13/2018
 * Time: 10:53 PM
 */

class m_booking extends CI_Model
{
    public function create($input){
        $this->db->trans_start();
        if(!isset($input['token'])){
            $input['token'] = '';
        }
        $guest = array(
            "guest_name"=>$input['guest_name'],
            "phone_number"=>$input['phone_number'],
            "email_address"=>$input['email_address'],
            "created_at"=>date('m/d/Y'),
            "address"=>$input['address']
        );
        $this->db->insert('guests', $guest);
        $guest_id = $this->db->insert_id();
        if($guest_id<=0){
            return 0;
        }
        $booking = array(
            "guest_id" => $guest_id,
            "check_in" => $input['check_in'],
            "check_out" => $input['check_out'],
            "booked_on" => date('m/d/Y'),
            "price" => 0,
            "gst"=>$this->config->item('gst'),
            "adult_count" => $input['adults'],
            "children_count" => $input['children'],
            "status" => "Unpaid",
            "token"=>$input['token']
        );
        $this->db->insert('bookings', $booking);
        $booking_id = $this->db->insert_id();
        $room_list = $input['rooms_list'];
        $rooms = explode(",",$room_list);
        $roomObjs = [];
        foreach ($rooms as $room){
            $roomObjs[]= array(
                "room_id"=>$room,
                "booking_id"=>$booking_id
            );
        }
        $this->db->insert_batch('reservations', $roomObjs);

        $this->db->trans_complete();
        if($this->db->trans_status()) {
            return $booking_id;
        }else{
            return 0;
        }
    }

    public function getBooking($id){
        $booking = $this->db->get_where('bookings', array("id"=>$id))->row();
        if($booking==null){
            return null;
        }
        $guest = $this->db->get_where('guests', array("id"=>$booking->guest_id))->row();
        $this->db->select('reservations.*, rooms.room_name, rooms.HSN, rooms.room_number, rooms.price, rooms.room_type');
        $this->db->join('rooms', 'reservations.room_id = rooms.id');
        $rooms = $this->db->get_where('reservations', array("booking_id"=>$booking->id))->result();
        return array(
            "booking" => $booking,
            "guest" => $guest,
            "rooms" => $rooms
        );
    }

    public function get($id){
        $booking = $this->db->get_where('bookings', array("id"=>$id))->row();
        if($booking==null){
            return null;
        }
        $guest = $this->db->get_where('guests', array("id"=>$booking->guest_id))->row();

        $this->db->select('reservations.*, rooms.room_name, rooms.HSN, rooms.room_number, rooms.price, rooms.room_type');
        $this->db->join('rooms', 'reservations.room_id = rooms.id');
        $rooms = $this->db->get_where('reservations', array("booking_id"=>$booking->id))->result();
        $booking->guest = $guest;
        $booking->rooms = $rooms;
        return $booking;
    }

    public function totalBooking(){
        $count = $this->db->query('select count(*) as count from bookings where status!=\'Cancelled\'')->row()->count;
        return $count;
    }

    public function futureBooking(){
        $count = $this->db->query('select count(*) as count from bookings where status!=\'Cancelled\' and check_in > convert(date, GETDATE())')->row()->count;
        return $count;
    }

    public function todayBooking(){
        $count = $this->db->query('select count(*) as count from bookings where status!=\'Cancelled\' and check_in = convert(date, GETDATE())')->row()->count;
        return $count;
    }

    public function all(){
        $this->db->join('guests', 'bookings.guest_id = guests.id');
        $this->db->select('bookings.*,guests.guest_name, guests.phone_number');
        $this->db->order_by('bookings.id', 'DESC');
        $this->db->order_by('bookings.check_in', 'ASC');
        $bookings = $this->db->get('bookings')->result();
        foreach ($bookings as $booking){
            $this->db->select('reservations.*, rooms.room_name, rooms.room_number, rooms.price');
            $this->db->join('rooms', 'reservations.room_id = rooms.id');
            $rooms = $this->db->get_where('reservations', array("booking_id"=>$booking->id))->result();
            $booking->rooms = $rooms;
        }
        return array(
            "bookings"=>$bookings
        );
    }
    public function allToday(){
        $this->db->join('guests', 'bookings.guest_id = guests.id');
        $this->db->select('bookings.*,guests.guest_name, guests.phone_number');
        $this->db->where('check_in = CONVERT(date, GETDATE())');
        $this->db->where('bookings.cancelled = 0');
        $this->db->where('bookings.checkin_time is null');
        $this->db->order_by('bookings.id', 'DESC');
        $this->db->order_by('bookings.check_in', 'ASC');
        $bookings = $this->db->get('bookings')->result();
        foreach ($bookings as $booking){
            $this->db->select('reservations.*, rooms.room_name, rooms.room_number, rooms.price');
            $this->db->join('rooms', 'reservations.room_id = rooms.id');
            $rooms = $this->db->get_where('reservations', array("booking_id"=>$booking->id))->result();
            $booking->rooms = $rooms;
        }
        return $bookings;
    }

    public function getBookingStat(){
        return '1';
        /*$dates = [];
        for($i=0;$i<7;$i++){
            $dates[]="'".date('Y-m-d', strtotime('-'.$i.' days'))."'";
        }
        $query = "select convert(date, booked_on) , count(*) from bookings where convert(date, booked_on) in (".explode(',', $dates).") group by convert(date, booked_on)";*/
    }
}