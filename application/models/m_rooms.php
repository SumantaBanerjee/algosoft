<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/12/2018
 * Time: 11:28 AM
 */

class m_rooms extends CI_Model
{
    public function add(){
        $room = $this->input->post();
        if(!isset($room['id'])) {
            $room_obj = array(
                "room_name" => $room['room_name'],
                "room_number" => $room['room_number'],
                "room_type" => $room['room_type'],
                "advance_price" => $room['advance_price'],
                "price" => $room['price'],
                "cancellation_fee" => $room['cancellation_fee'],
                "max_capacity" => $room['max_capacity'],
                "extension" => $room['extension'],
                "description" => $room['description']
            );
            $this->db->insert('rooms', $room_obj);
            $insert_id = $this->db->insert_id();
            $featureObjs = [];
            if ($insert_id > 0) {
                $features = $room['features'];
                foreach ($features as $feature => $val) {
                    $featureObjs[] = array(
                        "room_id" => $insert_id,
                        "feature_type" => $feature,
                        "availability" => 1
                    );
                }
                $this->db->insert_batch('room_features', $featureObjs);
                return $insert_id;
            } else {
                return 0;
            }
        }else{
            $features=[];
            $has_features = false;
            if(isset($room['features'])) {
                $features = $features = $room['features'];
                unset($room['features']);
                $has_features = true;
            }
            $id = $room['id'];
            log_message('error', 'Room id: '.$id);
            unset($room['id']);
            $this->db->where('id', $id);
            $this->db->update('rooms', $room);
            if($has_features) {
                foreach ($features as $feature => $val) {
                    $featureObjs[] = array(
                        "room_id" => $id,
                        "feature_type" => $feature,
                        "availability" => 1
                    );
                }
                $this->db->query('delete from room_features where room_id = ' . $id);
                $this->db->insert_batch('room_features', $featureObjs);
            }
            return $id;
        }
    }

    public function getVariations($id){
        $query = "select * from rooms where room_number = (select room_number from rooms where id = $id ) and id != $id";
        return $this->db->query($query)->result();
    }

    public function update(){

    }

    public function getPhotos($id){
        return $this->db->get_where('room_images', array("room_id"=>$id))->result();
    }

    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('rooms');
        $this->db->where('room_id', $id);
        $this->db->delete('room_features');
    }

    public function getById($id){
        $room = $this->db->get_where('rooms', array('id'=>$id))->row();
        if($room!=null){
            $features = $this->db->get_where('room_features', array('room_id' => $room->id))->result();
            $feature_list = [];
            foreach ($features as $feature){
                $feature_list[$feature->feature_type] = $feature->availability;
            }
            $result = array(
                "room"=>$room,
                "features"=>$feature_list
            );
            return $result;
        }else{
            return array();
        }
    }

    public function toggleMaintenance($id){
        $query = "update rooms set status = IIF(status = 'maintenance', 'available', 'maintenance') where room_number = (select room_number from rooms where id = $id)";
        $this->db->query($query);
        echo $this->db->affected_rows();
    }

    public function getBookings($id){
        $query = "select * from bookings where id in (select booking_id from reservations where room_id in (select id from rooms where room_number = (select room_number from rooms where id = $id)))";
        $bookings = $this->db->query($query)->result();
        return $bookings;
    }

    public function checkAvailability($room_id, $checkIn, $checkOut){
        //$query = 'select count(*) as count from reservations where booking_id in (select id from bookings where check_in >=\''.$checkIn.'\' and check_in<\''.$checkOut.'\') and cancelled = 0 and room_id = '.$room_id;
        //$query = "select * from rooms where room_number not in(select room_number from reservations join rooms on reservations.room_id = rooms.id join bookings on reservations.booking_id = bookings.id where check_in >= '$checkIn' and check)";
        $query = "select distinct room_number from room_availability join rooms on room_availability.room_id = rooms.id where date>='$checkIn' and date<'$checkOut' and cancelled = 0";
        log_message('error',$query);
        $count = $this->db->query($query)->row()->count;
        if($count>0){
            return false;
        }else{
            return true;
        }
    }

    public function getAll(){
        $this->db->order_by('room_number', 'desc');
        $rooms = $this->db->get('rooms')->result();
        foreach ($rooms as $room){
            $features = $this->db->get_where('room_features', array('room_id' => $room->id))->result();
            $images = $this->db->get_where('room_images', array('room_id' => $room->id))->result();
            $room->features = $features;
            $room->images = $images;
        }
        return $rooms;
    }
}