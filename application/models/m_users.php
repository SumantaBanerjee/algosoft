<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/16/2018
 * Time: 8:51 AM
 */

class m_users extends CI_Model
{
    public function login($userid, $password){
        $this->db->where('username', $userid);
        $this->db->where('password', $password);
        $this->db->where('enabled', 1);
        $user = $this->db->get('users')->row();
        if($user!=null){
            $this->db->where('id', $user->id);
            $this->db->update('users', array("last_login"=>date('Y-m-d')));
            return $user;
        }else{
            return null;
        }
    }

    public function add(){
        $user = $this->input->post();
        if(!isset($user['id'])){
            $this->db->insert('users', $user);
            return $this->db->insert_id();
        }else{
            $id = $user['id'];
            unset($user['id']);
            $this->db->where('id', $id)->update('users', $user);
            return $id;
        }
    }

    public function getById($id){
        return $this->db->get_where('users', array("id"=>$id))->row();
    }

    public function all(){
        return $this->db->get('users')->result();
    }

    public function getRoleForUser($id){
        return $this->db->where('id', $id)->get('users')->row()->role;
    }

    public function getModulesForRole($role){
        $modules = $this->db->join('project_structure', 'config_id = project_structure.id')->where('role_name', $role)->get('role_definitions')->result();
        return $modules;
    }
}