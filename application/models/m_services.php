<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/13/2018
 * Time: 11:12 AM
 */

class m_services extends CI_Model
{
    public function add(){
        $service = $this->input->post();
        $service['gst'] = $this->config->item('gst');
        if(isset($service['available'])){
            if($service['available']=='Yes'){
                $service['available'] = 1;
            }
        }else{
            $service['available'] = 0;
        }
        if(!isset($service['id'])) {
            $this->db->insert('service_catalog', $service);
            echo $this->db->insert_id();
        }else{
            $id = $service['id'];
            unset($service['id']);
            $this->db->where('id', $id);
            $this->db->update('service_catalog', $service);
            echo $id;
        }
    }
    public function getCategories(){
        $categories = $this->db->where('type', 'service_category')->get('lookups')->result();
        return $categories;
    }
    public function getById($id){
        $service = $this->db->get_where('service_catalog', array("id"=>$id))->row();
        return array("service"=>$service);
    }
    public function getAll(){
        return $this->db->get('service_catalog')->result();
    }
    public function getAllQuickServices(){
        $this->db->select('extra_services.*, rooms.room_number, rooms.room_name, service_catalog.service_name, service_catalog.total_price, service_catalog.service_price');
        $this->db->join('reservations', 'extra_services.room_id = reservations.id');
        $this->db->join('rooms', 'reservations.room_id = rooms.id');
        $this->db->join('bookings', 'bookings.id = extra_services.booking_id');
        $this->db->join('service_catalog', 'extra_services.service_id = service_catalog.id');
        $this->db->order_by('extra_services.service_id', 'DESC');
        $services = $this->db->get('extra_services')->result();
        return $services;
    }
    public function getQuickServices($booking){
        $this->db->select('extra_services.*, rooms.room_number, rooms.room_name, service_catalog.service_name, service_catalog.total_price, service_catalog.HSN, service_catalog.service_price');
        $this->db->join('reservations', 'extra_services.room_id = reservations.id');
        $this->db->join('rooms', 'reservations.room_id = rooms.id');
        $this->db->join('bookings', 'bookings.id = extra_services.booking_id');
        $this->db->join('service_catalog', 'extra_services.service_id = service_catalog.id');
        $this->db->where('extra_services.status != ','cancelled');
        $this->db->where('extra_services.booking_id', $booking);
        $this->db->order_by('extra_services.service_id', 'DESC');
        $services = $this->db->get('extra_services')->result();
        return $services;
    }
    public function getActiveQuickServices(){
        $this->db->select('extra_services.*, rooms.room_number, rooms.room_name, service_catalog.service_name, service_catalog.total_price, service_catalog.service_price');
        $this->db->join('reservations', 'extra_services.room_id = reservations.id');
        $this->db->join('rooms', 'reservations.room_id = rooms.id');
        $this->db->join('bookings', 'bookings.id = extra_services.booking_id');
        $this->db->join('service_catalog', 'extra_services.service_id = service_catalog.id');
        $this->db->order_by('extra_services.service_id', 'DESC');
        $this->db->where('extra_services.status !=', 'cancelled');
        $services = $this->db->get('extra_services')->result();
        return $services;
    }
}