<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/20/2018
 * Time: 10:48 AM
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nativesession
{
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function set_userdata($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function get($key)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    public function userdata($key)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }


    public function regenerateId($delOld = false)
    {
        session_regenerate_id($delOld);
    }

    public function delete($key)
    {
        unset($_SESSION[$key]);
    }

    public function sess_destroy(){
        session_destroy();
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }
}