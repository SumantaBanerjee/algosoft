<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('check_login')) {
    function check_login($context)
    {
        if (!$context->nativesession->userdata('logged_in')) {
            redirect('login');
        }
    }
}

if (!function_exists('get_configs_for_role')) {
    function check_module_access($context)
    {
        if($context->nativesession->userdata('role')=='admin'){
            return;
        }
        $controller = $context->uri->segment(1);
        $method = $context->uri->segment(2);
        $modules = $context->nativesession->userdata('modules');
        $has_access = false;
        foreach ($modules as $module) {
            if ($module->controller == $controller && $module->method == $method && $module->allowed == 1) {
                $has_access = true;
                break;
            }
        }
        if (!$has_access) {
            redirect('no_access');
        }
    }
}
