<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('load_menu')) {
    function load_menu()
    {
        return file_get_contents('../views/menu.php');
    }
}