<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/8/2018
 * Time: 12:35 PM
 */

class dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_login($this);
        $timezone = "Asia/Calcutta";
        if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
    }

    public function index(){
        $total = $this->m_booking->totalBooking();
        $future = $this->m_booking->futureBooking();
        $current = $this->m_booking->todayBooking();

        $booking_statistics = $this->m_booking->getBookingStat();

        $menu = $this->load->view('menu', '', true);

        $this->load->view('dashboard', array(
            "total_booking_count" => $total,
            "future_booking_count" => $future,
            "current_booking_count" => $current,
            "today_revenue" => $this->db->query("select COALESCE(sum(rooms.price),0) as total from reservations join bookings on reservations.booking_id = bookings.id join rooms on reservations.room_id = rooms.id where bookings.status='Paid' and check_in = convert(date, GETDATE())")->row()->total,
            "week_revenue"=>$this->db->query("select COALESCE(sum(rooms.price),0) as total from reservations join bookings on reservations.booking_id = bookings.id join rooms on reservations.room_id = rooms.id where bookings.status='Paid' and DATEPART(week, check_in) = DATEPART(week, GETDATE()) and YEAR(check_in) = YEAR(convert(date, GETDATE()))")->row()->total,
            "month_revenue"=> $this->db->query("select COALESCE(sum(rooms.price),0) as total from reservations join bookings on reservations.booking_id = bookings.id join rooms on reservations.room_id = rooms.id where bookings.status='Paid' and MONTH(check_in) = MONTH(convert(date, GETDATE())) and YEAR(check_in) = YEAR(convert(date, GETDATE()))")->row()->total,
            "year_revenue"=> $this->db->query("select COALESCE(sum(rooms.price),0) as total from reservations join bookings on reservations.booking_id = bookings.id join rooms on reservations.room_id = rooms.id where bookings.status='Paid' and YEAR(check_in) = YEAR(convert(date, GETDATE()))")->row()->total,
            "bookings" => $this->m_booking->allToday(),
            "service_rev_t"=>$this->db->query("select COALESCE(sum(cost),0) as total from extra_services join service_catalog on service_id = service_catalog.id where convert(date, extra_services.date) = convert(date, CURRENT_TIMESTAMP) and extra_services.status='completed'")->row()->total,
            "service_rev_w"=>$this->db->query("select COALESCE(sum(cost),0) as total from extra_services join service_catalog on service_id = service_catalog.id where DATEPART(week, extra_services.date) = DATEPART(week, GETDATE()) and YEAR(extra_services.date) = YEAR(convert(date, GETDATE())) and extra_services.status='completed'")->row()->total,
            "service_rev_m"=>$this->db->query("select COALESCE(sum(cost),0) as total from extra_services join service_catalog on service_id = service_catalog.id where MONTH(extra_services.date) = MONTH(convert(date, GETDATE())) and YEAR(extra_services.date) = YEAR(convert(date, GETDATE())) and extra_services.status='completed'")->row()->total,
            "service_rev_y"=>$this->db->query("select COALESCE(sum(cost),0) as total from extra_services join service_catalog on service_id = service_catalog.id where YEAR(extra_services.date) = YEAR(convert(date, GETDATE())) and extra_services.status='completed'")->row()->total,
            "services" => $this->m_services->getActiveQuickServices(),
            "menu_bar" => $menu
        ));
    }

}