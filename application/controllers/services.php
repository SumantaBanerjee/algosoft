<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/12/2018
 * Time: 10:11 AM
 */

class services extends CI_Controller
{
    public function add($param1 = '')
    {
        $menu = $this->load->view('menu', '', true);
        if ($this->input->method() == "post") {
            echo $this->m_services->add();
        } else {
            if ($param1 == '') {
                $this->load->view('add_service', array("menu_bar" => $menu));
            } else {
                $service = $this->m_services->getById($param1);
                $service['menu_bar'] = $menu;
                $this->load->view('add_service', $service);
            }
        }
    }

    public function all()
    {
        $services = $this->m_services->getAll();
        $menu = $this->load->view('menu', '', true);
        $data = array(
            "services" => $services,
            "menu_bar" => $menu
        );
        $this->load->view('all_services', $data);
    }

    public function view_qs($param1)
    {
        $this->db->select('extra_services.*, rooms.room_number, rooms.room_name, service_catalog.service_name, service_catalog.total_price, service_catalog.service_price');
        $this->db->join('reservations', 'extra_services.room_id = reservations.id');
        $this->db->join('rooms', 'reservations.room_id = rooms.id');
        $this->db->join('bookings', 'bookings.id = extra_services.booking_id');
        $this->db->join('service_catalog', 'extra_services.service_id = service_catalog.id');
        $this->db->where('extra_services.id', $param1);
        $service = $this->db->get('extra_services')->row();
        $menu = $this->load->view('menu', '', true);
        $this->load->view('view_qs', array("service" => $service, "menu_bar" => $menu));
    }

    public function quick_services()
    {
        $services = $this->m_services->getAllQuickServices();
        //Load view
        $menu = $this->load->view('menu', '', true);
        $this->load->view('quick_services', array("services" => $services, "menu_bar" => $menu));
    }

    public function qs_stat($qs_id, $stat)
    {
        if ($stat == "completed") {
            $this->db->where('status', 'pending');
            $this->db->where('id', $qs_id);
            $this->db->update('extra_services', array("status" => $stat));
        } else if ($stat == "cancelled") {
            $this->db->where('status', 'pending');
            $this->db->where('id', $qs_id);
            $this->db->update('extra_services', array("status" => $stat));
        }
        redirect('services/quick_services');
    }

    public function quick($param1 = '')
    {
        if ($this->input->method() == "post") {
            $service = $this->input->post();
            if(isset($service['complementary']) && $service['complementary']==1){
                $service['cost'] = 0;
                $service['cost_with_gst'] = 0;
            }
            $this->db->insert('extra_services', $service);
            $service_id = $this->db->insert_id();
            redirect('services/quick_services');
        }
        $booking = $this->m_booking->get($param1);
        $menu = $this->load->view('menu', '', true);
        $this->load->view('quick_service', array("booking" => $booking, "services" => $this->m_services->getAll(), "menu_bar" => $menu, "categories" => $this->m_services->getCategories()));
    }
}