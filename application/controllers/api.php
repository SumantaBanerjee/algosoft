<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/14/2018
 * Time: 10:03 PM
 */

class api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->output->set_content_type('application/json');
    }

    public function getRooms(){
        $rooms = $this->m_rooms->getAll();
        echo json_encode(array("rooms"=>$rooms));
    }

    public function availability(){
        $in = $this->input->post('check_in');
        $out = $this->input->post('check_out');
        $room = $this->input->post('room_id');
        $availability = $this->m_rooms->checkAvailability($room, $in, $out);
        if($availability==true){
            echo 1;
        }else{
            echo 0;
        }
    }

    public function all(){
        $token = $this->input->post('token');
        $this->db->where('bookings.token', $token);
        $this->db->join('guests', 'bookings.guest_id = guests.id');
        $this->db->select('bookings.*,guests.guest_name, guests.phone_number');
        $this->db->order_by('bookings.id', 'DESC');
        $this->db->order_by('bookings.check_in', 'ASC');
        $bookings = $this->db->get('bookings')->result();
        foreach ($bookings as $booking){
            $this->db->select('reservations.*, rooms.room_name, rooms.room_number, rooms.price');
            $this->db->join('rooms', 'reservations.room_id = rooms.id');
            $rooms = $this->db->get_where('reservations', array("booking_id"=>$booking->id))->result();
            $booking->rooms = $rooms;
        }

        echo json_encode(array(
            "bookings"=>$bookings
        ));
    }

    public function book(){
        $input = $this->input->post();
        $booking_id = $this->m_booking->create($input);
        $days = $input['days'];
        $guest_name = $input['guest_name'];
        $email_address = $input['email_address'];
        $checkin = $input['check_in'];
        $checkout = $input['check_in'];
        $guest_count = $input['adults'] + $input['children'];
        $room_list = $input['rooms_list'];
        $rooms = explode(",",$room_list);
        $room_count = sizeof($rooms);
        $final_room_names=[];
        $final_rooms = [];
        $base_cost = 0;
        $room_prices = [];
        foreach ($rooms as $room){
            $room_data = $this->m_rooms->getById($room);
            $final_rooms[]=$room_data['room'];
            $final_room_names[]=$room_data['room']->room_name;
            $base_cost+=$room_data['room']->price;
            $room_prices[] = 'Rs. '.$room_data['room']->price;
        }
        $base_cost = $base_cost*$days;
        $room_names = implode($final_room_names,", ");
        $template = $this->config->item('email_template_web');
        $gst = $this->config->item('gst')*$base_cost/100;
        $mail = sprintf($template,
            $guest_name,
            $room_names,
            $booking_id,
            $checkin,
            $checkout,
            $this->config->item('hotel_address'),
            $this->config->item('landmark'),
            '',
            $base_cost+$gst,
            date('M d',strtotime($checkin)),
            date('M d',strtotime($checkout)),
            $guest_count,
            $room_count,
            $this->config->item('phone'),
            implode($room_prices,' + '),
            $base_cost,
            $gst,
            $base_cost+$gst
        );
        $this->sendMail($mail, $email_address);
        $sms = $this->config->item('sms_text');
        $this->sendSms($input['phone_number'],
            sprintf($sms,
                $guest_name,
                $room_names,
                $base_cost+$gst,
                $this->config->item('hotel_address'),
                $this->config->item('landmark'),
                $this->config->item('map'))
        );
        echo $booking_id;
    }

    private function sendSms($to, $text)
    {
        $curl = curl_init();
        $authKey = $this->config->item('auth_key');
        $senderId = $this->config->item('sender_id');
        $text = urlencode($text);
        $url = "http://msg.algosoft.co.in/rest/services/sendSMS/sendGroupSms?AUTH_KEY=$authKey&message=$text&senderId=$senderId&routeId=1&mobileNos=$to&smsContentType=english";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 906ad805-3a40-ff92-b709-f64820cd0f2f"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $response;
    }


    public function sendMail($message, $to)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_port' => 587,
            'smtp_user' => 'apikey', // change it to yours
            'smtp_pass' => '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', // change it to yours
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline('\r\n');
        $this->email->from('Hotel Rohini International <support@rohiniintl.com>'); // change it to yours
        $this->email->to($to);// change it to yours
        $this->email->subject('Your booking has been confirmed');
        $this->email->message($message);
        if($this->email->send()){}

    }
}