<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/13/2018
 * Time: 12:47 PM
 */

class booking extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    public function create(){
        check_module_access($this);
        if($this->input->method()=="post"){
            $input = $this->input->post();
            $booking_id = $this->m_booking->create($input);
            //get booking and send email
            $guest_name = $input['guest_name'];
            $email_address = $input['email_address'];
            $checkin = $input['check_in'];
            $checkout = $input['check_out'];
            $guest_count = $input['adults'] + $input['children'];
            $room_list = $input['rooms_list'];
            $rooms = explode(",",$room_list);
            $room_count = sizeof($rooms);
            $final_room_names=[];
            $final_rooms = [];
            $base_cost = 0;
            $day_count = date_diff(date_create($checkin),date_create($checkout))->format("%d");
            $room_prices = [];
            foreach ($rooms as $room){
                $room_data = $this->m_rooms->getById($room);
                $final_rooms[]=$room_data['room'];
                $final_room_names[]=$room_data['room']->room_name;
                $base_cost+=$room_data['room']->price;
                $room_prices[] = 'Rs. '.$room_data['room']->price;
            }
            echo 'Day count: '.$day_count.'<br/>';
            $base_cost = $base_cost*$day_count;
            $room_names = implode($final_room_names,", ");
            $template = $this->config->item('email_template_web');
            $gst = $this->config->item('gst')*$base_cost/100;

            //starting from check in till (day_count-1) insert into the room_availability table
            $invoices = [];
            $room_availability = [];
            foreach ($rooms as $room){
                $room_availability[]=array(
                    "booking_id"=>$booking_id,
                    "room_id" => $room,
                    "date"=>$checkin
                );
                /*$invoices[] = array(
                    "booking_id"=>$booking_id,
                    "item_id"=>$room,
                    "item_type"=>"room",
                    "price"->
                )*/
            };

            for($i=1;$i<$day_count;$i++){
                $date = DateTime::createFromFormat('Y-m-d', $checkin)->modify('+'.$i.' day')->format('Y-m-d');
                foreach ($rooms as $room){
                    $room_availability[]=array(
                        "booking_id"=>$booking_id,
                        "room_id" => $room,
                        "date"=>$date
                    );
                };
            }

            $this->db->insert_batch('room_availability', $room_availability);

            $checkin = date('d/m/Y', strtotime($checkin));
            $checkout = date('d/m/Y', strtotime($checkout));

            $mail = sprintf($template,
                $guest_name,
                $room_names,
                $booking_id,
                $checkin.' 12:00 PM',
                $checkout.' 11:00 AM',
                $this->config->item('hotel_address'),
                $this->config->item('landmark'),
                '',
                $base_cost+$gst,
                date('M d',strtotime($checkin)),
                date('M d',strtotime($checkout)),
                $guest_count,
                $room_count,
                $this->config->item('phone'),
                implode($room_prices,' + '),
                $base_cost,
                $gst,
                $base_cost+$gst
            );
            $this->sendMail($mail, $email_address);
            $sms = $this->config->item('sms_text');
            $sms_text = sprintf($sms,
                $guest_name,
                $room_names,
                $base_cost+$gst,
                $this->config->item('hotel_address'),
                $this->config->item('landmark'),
                $this->config->item('map'));
            //$this->sendSms($input['phone_number'], $sms_text);
            log_message('ERROR', $sms_text);
            echo $sms_text;
            //redirect('booking/view/'.$booking_id);
        }else {
            $menu = $this->load->view('menu', '', true);
            $this->load->view('new_reservation', array("menu_bar"=>$menu));
        }
    }


    private function sendSms($to, $text)
    {
        $curl = curl_init();
        $authKey = $this->config->item('auth_key');
        $senderId = $this->config->item('sender_id');
        $text = urlencode($text);
        $url = "http://msg.algosoft.co.in/rest/services/sendSMS/sendGroupSms?AUTH_KEY=$authKey&message=$text&senderId=$senderId&routeId=1&mobileNos=$to&smsContentType=english";
        //$url = "http://jsonplaceholder.typicode.com/posts/1";
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 906ad805-3a40-ff92-b709-f64820cd0f2f"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $response;
    }


    public function sendMail($message, $to)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_port' => 587,
            'smtp_user' => 'apikey', // change it to yours
            'smtp_pass' => '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', // change it to yours
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline('\r\n');
        $this->email->from('Hotel Rohini International <support@rohiniintl.com>'); // change it to yours
        $this->email->to($to);// change it to yours
        $this->email->subject('Your booking has been confirmed');
        $this->email->message($message);
        if($this->email->send())
        {

        }
        else
        {

        }

    }

    public function checkin($booking_id){
        $this->db->where('id', $booking_id);
        $this->db->update('bookings', array(
            "checkin_time" => date('h:i:s A')
        ));
        if($this->db->affected_rows()>0){
            echo 1;
        }else{
            echo 0;
        }
    }

    public function checkout($id){
        $this->db->where('id', $id);
        $this->db->update('bookings', array(
            "checkout_time" => date('h:i:s A'),
            "check_out" => date('Y-m-d')
        ));
        if($this->db->affected_rows()>0){
            echo $this->markpaid($id);
        }else{
            echo 0;
        }
    }

    public function all(){
        check_module_access($this);
        $bookings = $this->m_booking->all();
        $menu = $this->load->view('menu','',true);
        $bookings['menu_bar']=$menu;
        $bookings['services'] = $this->m_services->getAll();
        $this->load->view('all_bookings', $bookings);
    }

    public function view($id){
        check_module_access($this);
        $booking = $this->m_booking->getBooking($id);
        $menu = $this->load->view('menu','',true);
        $booking['menu_bar'] = $menu;
        $this->load->view("view_booking", $booking);
    }
    //Changed logic
    public function cancelreservation($reservation_id, $fee, $sendMail){
        check_module_access($this);
        $this->db->trans_start();
        $this->db->where('id', $reservation_id);
        /*$this->db->delete('reservations');*/
        $this->db->update('reservations', array(
            "cancelled" => 1,
            "cancellation_fee" => $fee
        ));
        $reservations = $this->db->query("select * from reservations where id in (select id from reservations where booking_id = (select booking_id from reservations where id = $reservation_id ))")->result();
        $reservation_count = sizeof($reservations);
        $active_reservation_count = 0;
        foreach ($reservations as $reservation){
            if($reservation->cancelled!=1){
                $active_reservation_count++;
            }
        }
        if($reservation_count>0 && $active_reservation_count<=0) {
            $this->db->where('id', $reservations[0]->booking_id);
            $this->db->update('bookings', array('status' => 'Cancelled'));
        }
        $this->db->where('booking_id', $reservations[0]->booking_id);
        $this->db->where('room_id',$reservations[0]->room_id);
        $this->db->update('room_availability', array('cancelled'=>1));
        $this->db->trans_complete();
        if($this->db->trans_status()) {
            echo 1;
        }else{
            echo 0;
        }
    }

    public function markpaid($bid){
        check_module_access($this);
        $this->db->where('id', $bid);
        $this->db->update('bookings', array('status'=>'Paid'));
        echo 1;
    }
    public function cancel($bid){
        check_module_access($this);
        $this->db->trans_start();
        $this->db->where('id', $bid);
        $this->db->update('bookings', array('status'=>'Cancelled'));
        $this->db->where('booking_id', $bid);
        $this->db->update('room_availability', array('cancelled'=>1));
        $this->db->trans_complete();
        if($this->db->trans_status()) {
            echo 1;
        }else{
            echo 0;
        }
    }
    public function invoice($id){
        $booking = $this->m_booking->getBooking($id);
        if(!isset($booking['booking']->invoice_date) || $booking['booking']->invoice_date==null){
            $this->db->where('id', $id)->update('bookings', array("invoice_date"=>date('Y-m-d')));
        }
        //1 rooms
        //2 services
        $menu = $this->load->view('menu','',true);
        $booking['booking']->invoice_date = date('Y-m-d');
        $this->load->view('invoice', array("menu_bar"=>$menu, "booking"=>$booking, "services"=>$this->m_services->getQuickServices($id)));
    }
    public function serviceinvoice($id){
        $booking = $this->m_booking->getBooking($id);
        if(!isset($booking->invoice_date) || $booking->invoice_date==null){
            $this->db->where('id', $id)->update('bookings', array("invoice_date"=>date('Y-m-d')));
        }
        //1 rooms
        //2 services
        $menu = $this->load->view('menu','',true);
        $this->load->view('service_invoice', array("menu_bar"=>$menu, "booking"=>$booking, "services"=>$this->m_services->getQuickServices($id)));
    }
}