<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/27/2018
 * Time: 9:27 AM
 */

class migrate extends CI_Controller
{
    public function upgrade(){
        $this->load->dbforge();

        /*try {
            $fields = array(
                'status' => array('type' => 'VARCHAR', 'constraint' => 15, 'default' => 'available')
            );
            $this->dbforge->add_column('rooms', $fields);
        }catch (Exception $ex){

        }*/

        $table_name = "invoice_items";
        $fields = array(
            "id" => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            "invoice_id" => array(
                'type' => 'INT'
            ),
            "item_id" => array(
                'type' => 'INT'
            ),
            "item_type" => array(
                'type' => 'VARCHAR',
                'constraint' => 60
            ),
            "created_at datetime default getdate()"
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($table_name);
        echo "Table created";
    }
    public function rollback(){
        $this->load->dbforge();
        $table_name = "invoice_items";
        $this->dbforge->drop_table($table_name, TRUE);
        echo "Table dropped";
    }
}