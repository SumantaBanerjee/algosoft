<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/10/2018
 * Time: 11:17 AM
 */

class rooms extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    public function add($param1=''){
        check_module_access($this);
        $menu = $this->load->view('menu','',true);
        if($this->input->method()=='post'){
            echo $this->m_rooms->add();
        }else {
            if($param1!='') {
                $room = $this->m_rooms->getById($param1);
                $room['menu_bar'] = $menu;
                $this->load->view('add_room', $room);
            }else{
                $this->load->view('add_room', array("menu_bar"=>$menu));
            }
        }
    }

    public function toggleMaintenance($id){
        check_module_access($this);
        $this->m_rooms->toggleMaintenance($id);
        redirect('rooms/all');
    }

    public function createVariation($id){
        check_module_access($this);
        if($this->input->method()=='post'){
            echo $this->m_rooms->add();
        }else {
            $menu = $this->load->view('menu', '', true);
            $room = $this->m_rooms->getById($id);
            $room['menu_bar'] = $menu;
            $this->load->view('create_variation', $room);
        }
    }

    public function all(){
        check_module_access($this);
        $rooms = $this->m_rooms->getAll();
        $menu = $this->load->view('menu','',true);
        $data = array('rooms'=>$rooms, "menu_bar"=>$menu);

        $this->load->view('all_rooms',$data);
    }

    public function getRoomById($id){
        check_module_access($this);
        $room = $this->m_rooms->getById($id);
        $room['variations'] = $this->m_rooms->getVariations($id);
        echo json_encode($room);
    }

    public function getAvailableRooms($from, $to){
        $from  =str_replace("-","/",$from);
        $to = str_replace("-","/",$to);
        $query1 = "select booking_id from bookings where check_in >= '".$from."' and check_in < '".$to."'";
        $query2 = "select room_id from reservations where cancelled!=1 and booking_id in (".$query1.")";
        $query3 = "select * from rooms where id not in (".$query2.")";
        $rooms = $this->db->query($query3)->result();
        echo json_encode($rooms);
    }

    public function photo($id){
        $room = $this->m_rooms->getById($id);
        $menu = $this->load->view('menu','',true);
        $data=array(
            "room" => $room['room'],
            "photos" => $this->m_rooms->getPhotos($id),
            "menu_bar"=>$menu
        );
        $this->load->view('photos', $data);
    }

    public function delete($room_id){
        $this->m_rooms->delete($room_id);
        echo 1;
    }
    // View overall availability
    public function status(){
        date_default_timezone_set('Asia/Kolkata');
        $menu = $this->load->view('menu','',true);
        $room_numbers = $this->db->query("select distinct(room_number) from rooms")->result();
        $data=array(
            "menu_bar"=>$menu,
            "room_numbers" => $room_numbers
        );
        $date = isset($_GET['date'])?$_GET['date']:date('d-m-Y');
        $date = DateTime::createFromFormat('d-m-Y', $date)->format('m/d/Y');
        $rooms = $this->db->get('rooms')->result();
        $query="select * from rooms where room_number not in (select room_number from room_availability join rooms on room_availability.room_id = rooms.id where date = '$date' and cancelled = 0)";
        $available_rooms = $this->db->query($query)->result();
        $data['all_rooms'] = $rooms;
        $data['available_rooms'] = $this->db->where('date != ', $date);
        $available_room_numbers = [];
        foreach ($available_rooms as $room){
            $available_room_numbers[] = $room->room_number;
        }
        $data['available_room_numbers'] = $available_room_numbers;
        $this->load->view('overall_roomstat', $data);
    }

    public function checkAvailability($id){
        $bookings = $this->m_rooms->getBookings($id);
        $menu = $this->load->view('menu','',true);
        $room = $this->m_rooms->getById($id);
        $data=array(
            "bookings" => $bookings,
            "menu_bar" => $menu,
            "room"=>$room
        );
        $this->load->view("room_availability", $data);
    }
}