<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/16/2018
 * Time: 8:59 AM
 */

class login extends CI_Controller
{
    public function index(){
        if($this->nativesession->userdata('logged_in')){
            redirect('dashboard');
        }
        if($this->input->method()=="post") {
            $username = $this->input->post("username");
            $password = $this->input->post("pass");
            $user = $this->m_users->login($username, $password);
            if ($user != null) {
                $_SESSION['logged_in'] = true;
                $this->nativesession->set_userdata('username', $username);
                $this->nativesession->set_userdata('full_name', $user->full_name);
                $this->nativesession->set_userdata('user_name', $user->full_name);
                session_start();
                $_SESSION['user_name'] = $user->full_name;
                $this->nativesession->set_userdata('user', $user);
                $this->nativesession->set_userdata('role', $user->role);
                $modules = $this->m_users->getModulesForRole($user->role);
                $this->nativesession->set_userdata('modules', $modules);
                /*$this->session->set_userdata('username', $username);
                $this->session->set_userdata('full_name', $user->full_name);
                $this->session->set_userdata('user_name', $user->full_name);
                session_start();
                $_SESSION['user_name'] = $user->full_name;
                $this->session->set_userdata('user', $user);
                $this->session->set_userdata('role', $user->role);
                $modules = $this->m_users->getModulesForRole($user->role);
                $this->session->set_userdata('modules', $modules);*/
                redirect('dashboard');
            }
        }
        $this->load->view('login');
    }
}