<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/25/2018
 * Time: 8:29 AM
 */

class restaurant extends CI_Controller
{
    public function index(){
        $menu = $this->load->view('menu','',true);
        $catalog = $this->db->get('restaurant_menu')->result();
        $categories = $this->db->distinct()->select('category')->get('restaurant_menu')->result();
        $by_category = array();
        foreach ($categories as $category){
            $items = $this->db->where("category", $category->category)->get('restaurant_menu')->result();
            $by_category[$category->category] = $items;
        }
        $by_category['All'] = $catalog;
        $categories[] = (object)array('category'=>'All');
        $data=array(
            "menu_bar" => $menu,
            "categories"=>$categories,
            "catalog" => $catalog,
            "by_category" => $by_category
        );
        $this->load->view('restaurant_pos', $data);
    }
}