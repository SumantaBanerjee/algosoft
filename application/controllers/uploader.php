<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/17/2018
 * Time: 9:08 AM
 */

class uploader extends CI_Controller
{
    public function upload1()
    {
        $ds = DIRECTORY_SEPARATOR;  //1
        $storeFolder = './uploads';   //2
        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $targetPath = dirname(__FILE__) . $ds . $storeFolder . $ds;  //4
            $targetFile = $targetPath . uniqid() . $_FILES['file']['name'];  //5
            move_uploaded_file($tempFile, $targetFile); //6
        }
    }

    public function up(){
        log_message('ERROR', json_encode($this->input->post()));
    }

    public function upload2()
    {
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 10240;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);


    }

    public function upload()
    {
        $data = array();
        $room_id = $this->input->post('room_id');
        if (!empty($_FILES['file']['name'])) {
            $filesCount = count($_FILES['file']['name']);
            for ($i = 0; $i < $filesCount; $i++) {
                $_FILES['uploadFile']['name'] = str_replace(",","_",$_FILES['file']['name'][$i]);
                $_FILES['uploadFile']['type'] = $_FILES['file']['type'][$i];
                $_FILES['uploadFile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                $_FILES['uploadFile']['error'] = $_FILES['file']['error'][$i];
                $_FILES['uploadFile']['size'] = $_FILES['file']['size'][$i];
                //Directory where files will be uploaded
                $uploadPath = 'uploads/';
                $config['upload_path'] = $uploadPath;
                $config['encrypt_name'] = true;
                // Specifying the file formats that are supported.
                $config['allowed_types'] = 'jpg|png|pdf|doc|docx|xls|xlsx|ppt|pptx|txt|rtf';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('uploadFile')) {
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                }
            }
            if (!empty($uploadData)) {
                $list=[];
                foreach ($uploadData as $value) {
                    $list[] = array(
                        'room_id' => $room_id,
                        'image_url' => base_url().'uploads/'.$value['file_name']
                    );
                }
                $this->db->insert_batch('room_images', $list);
                echo '1';
            }
        }
    }
}