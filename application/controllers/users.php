<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/16/2018
 * Time: 9:50 AM
 */

class users extends CI_Controller
{
    public function all(){
        check_module_access($this);
        $users = $this->m_users->all();
        $menu = $this->load->view('menu','',true);
        $this->load->view('all_users', array("users"=>$users, "menu_bar"=>$menu));
    }
    public function add($param1=''){
        check_module_access($this);
        $roles = $this->db->query("Select distinct(role) as role from users")->result();
        $menu = $this->load->view('menu','',true);
        $data = array(
            "roles" => $roles,
            "menu_bar"=>$menu
        );
        if($param1=='') {
            if ($this->input->method() == "post") {
                $user_id = $this->m_users->add();
                redirect('users/add/' . $user_id);
            }else{
                $this->load->view('add_user', $data);
            }
        }else {
            if ($this->input->method() == "post") {
                $user_id = $this->m_users->add();
                redirect('users/add/' . $user_id);
            }else{
                $user = $this->m_users->getById($param1);
                $data['user'] = $user;
                $this->load->view('add_user', $data);
            }
        }
    }
    public function modules($param1){
        check_module_access($this);
        $role = $this->m_users->getRoleForUser($param1);
        if($this->input->method()=="post"){
            $accessed_modules = $this->input->post("accessed_modules");
            $this->db->where('role_name', $role);
            $this->db->delete('role_definitions');
            $updateObj = [];
            foreach ($accessed_modules as $module_id){
                $updateObj[] = array(
                    "role_name" => $role,
                    "config_id" => $module_id
                );
            }
            $this->db->insert_batch('role_definitions', $updateObj);
            redirect('users/modules/'.$param1);
        }

        $modules = $this->m_users->getModulesForRole($role);
        $enabled_modules = [];
        foreach ($modules as $module){
            $enabled_modules[] = $module->config_id;
        }
        $structures = [];
        $controllers = $this->db->query("select distinct(controller) as controller from project_structure")->result();
        $controller_names = [];
        foreach ($controllers as $controller){
            $controller_names[] = $controller->controller;
            $_modules = $this->db->get_where('project_structure', array('controller'=>$controller->controller, 'exposed'=>1))->result();
            $structures[$controller->controller]=$_modules;
        }
        $menu = $this->load->view('menu','',true);
        $data = array(
            "role_name" => $role,
            "modules" => $modules,
            "structure" => $structures,
            "controllers" => $controller_names,
            "enabled_modules" => $enabled_modules,
            "menu_bar" => $menu
        );
        $this->load->view('modules', $data);
    }

}