<?php
/**
 * Created by PhpStorm.
 * User: Sumanta Banerjee
 * Date: 5/14/2018
 * Time: 11:25 AM
 */

class profile extends CI_Controller
{
    public function index(){
        $user_id = $this->session->userdata('user')->id;
        redirect('users/add/'.$user_id);
    }
}