<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title><?= $this->config->item('hotel_name') ?></title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="<?= base_url() ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!--bootstrap -->
    <link href="<?= base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/material/material.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/material_style.css"/>
    <link href="<?= base_url() ?>/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css"/>
    <!-- animation -->
    <link href="<?= base_url() ?>/assets/css/pages/animate_page.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.css"/>
    <!-- Template Styles -->
    <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/theme-color.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/plugins/getmdl-select/material.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/plugins/getmdl-select/getmdl-select.min.css" rel="stylesheet" type="text/css"/>
    <!-- favicon -->
    <link rel="shortcut icon" href="./assets/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <!-- logo start -->
            <div class="page-logo">
                <a href="<?= base_url() ?>index.php/dashboard">
                    <img alt="" src="./assets/img/logo.png"/>
                    <span class="logo-default"><?= $this->config->item('software_name') ?></span> </a>
            </div>
            <!-- logo end -->
            <ul class="nav navbar-nav navbar-left in">
                <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
            </ul>
            <!-- start mobile menu -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- end mobile menu -->
            <!-- start header menu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle " src="<?= base_url() ?>assets/img/dp.jpg"/>
                            <span class="username username-hide-on-mobile"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default animated fadeIn">
                            <li>
                                <a href="<?= base_url() ?>index.php/profile">
                                    <i class="icon-user"></i> Profile </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/settings">
                                    <i class="icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        <?= $menu_bar ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Quick Service Details</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                                   href="<?= base_url() ?>index.php/services/all">Services</a>&nbsp;<i
                                        class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Quick Service</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <div class="row">
                    <form method="post" id="service_form">
                        <?php if (isset($booking)) { ?>
                            <input hidden value="<?= $booking->id ?>" name="booking_id">
                        <?php } ?>
                        <div class="col-sm-12">
                            <div class="card-box">
                                <div class="card-head">
                                    <header>Quick Service for Booking #<?= $booking->id ?></header>
                                </div>
                                <div class="card-body row">
                                    <div class="col-lg-12 p-t-20">
                                        <!--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width" id="roomList">
                                            <input class="mdl-textfield__input" type="text" id="list3" readonly="" tabindex="-1" />
                                            <label for="list3" class="pull-right margin-0">
                                                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                            </label>
                                            <label for="list3" class="mdl-textfield__label">Select Room No</label>
                                            <ul data-mdl-for="list3" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                <?php /*foreach ($booking->rooms as $room) {*/ ?>
                                                <li class="mdl-menu__item" data-val="<? /*= $room->id */ ?>" id="val<? /*= $room->id */ ?>">Room <? /*= $room->room_number*/ ?></li>
                                                <?php /*} */ ?>
                                            </ul>
                                        </div>
                                        <input id="proxy_room" name="room_id" hidden>-->
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                                            <input type="text" value="" class="mdl-textfield__input" id="sample5"
                                                   readonly>
                                            <input type="hidden" value="" name="room_id">
                                            <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                            <label for="sample5" class="mdl-textfield__label">Derived Room</label>
                                            <ul for="sample5" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                <?php foreach ($booking->rooms as $room) { ?>
                                                    <li class="mdl-menu__item" data-val="<?= $room->id ?>">
                                                        Room <?= $room->room_number ?></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                                            <input type="text" value="" class="mdl-textfield__input" id="sample6"
                                                   readonly>
                                            <input type="hidden" value="" name="service_id" id="sample6_val">
                                            <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                            <label for="sample6" class="mdl-textfield__label">Choose a service</label>
                                            <ul for="sample6" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                <?php foreach ($services as $service) { ?>
                                                    <li class="mdl-menu__item"
                                                        data-val="<?= $service->id ?>"><?= $service->service_name ?></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <!--<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                                            <input class="mdl-textfield__input" type="text" id="list4" readonly="" tabindex="-1" />
                                            <label for="list4" class="pull-right margin-0">
                                                <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                            </label>
                                            <label for="list4" class="mdl-textfield__label">Select Service</label>
                                            <ul data-mdl-for="list4" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                <?php /*foreach ($services as $service) {*/ ?>
                                                    <li class="mdl-menu__item" data-val="<? /*= $service->id */ ?>"><? /*= $service->service_name*/ ?></li>
                                                <?php /*} */ ?>
                                            </ul>
                                        </div>-->
                                        <!--<input id="proxy_service" name="service_id" hidden>-->
                                    </div>


                                    <div class="col-lg-6 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                                            <input type="text" value="" class="mdl-textfield__input" id="sample7"
                                                   readonly>
                                            <input type="hidden" value="" name="category" id="sample7val">
                                            <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                            <label for="sample7" class="mdl-textfield__label">Choose Extra category</label>
                                            <ul for="sample7" class="mdl-menu mdl-menu--bottom-left mdl-js-menu"
                                                id="sample7list">
                                                <?php foreach ($categories as $cat) { ?>
                                                    <li class="mdl-menu__item" data-val="<?= $cat->val ?>"><?= $cat->val ?></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" type="number" name="cost"
                                                   id="service_price" value="0" required/>
                                            <label class="mdl-textfield__label" for="service_price">Price</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" id="tax" type="text"
                                                   pattern="-?[0-9]*(\.[0-9]+)?" value="0" required/>
                                            <label class="mdl-textfield__label" for="tax">Extra taxes</label>
                                            <span class="mdl-textfield__error">Number required!</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" name="cost_with_gst" type="text"
                                                   pattern="-?[0-9]*(\.[0-9]+)?" id="total_price" value="0" required
                                                   readonly/>
                                            <label class="mdl-textfield__label" for="total_price">Price with GST</label>
                                            <span class="mdl-textfield__error">Number required!</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                            <label class="mdl-checkbox mdl-js-checkbox" for="checkbox1">
                                                <input type="checkbox" id="checkbox1" class="mdl-checkbox__input"
                                                       name="complementary" value="1"/>
                                                <span class="mdl-checkbox__label">Included in rate plan as complimentary <i
                                                            class="fa fa-question-circle"
                                                            title="This option will create the extra service for the booking with 0 value and will not add up in the invoice amount."></i> </span>
                                            </label>

                                        </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                            <label class="mdl-checkbox mdl-js-checkbox" for="checkbox2">
                                                <input type="checkbox" id="checkbox2" class="mdl-checkbox__input"
                                                       name="hide" value="1"/>
                                                <span class="mdl-checkbox__label">Hide this item in invoice <i
                                                            class="fa fa-question-circle"
                                                            title="When this is enabled, the service price won't add up in the invoice."></i> </span>
                                            </label>

                                        </div>
                                    </div>
                                    <div class="col-lg-12 p-t-20 text-center">
                                        <button type="submit"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">
                                            Submit
                                        </button>
                                        <button type="reset"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end page content -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> <?= date('Y') ?> &copy; Developed by
            <a href="#" target="_top" class="makerCss">Algosoft</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- end footer -->
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/popper/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Common js-->
<script src="<?= base_url() ?>/assets/js/app.js"></script>
<script src="<?= base_url() ?>/assets/js/layout.js"></script>
<script src="<?= base_url() ?>/assets/js/theme-color.js"></script>
<!-- Material -->
<script src="<?= base_url() ?>/assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>/assets/js/pages/ui/animations.js"></script>

<!-- Sweet Alert -->
<script src="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>/assets/js/pages/sweet-alert/sweet-alert-data.js"></script>
<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/material_select/getmdl-select.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/datetimepicker.js"></script>
<script defer src="<?= base_url() ?>assets/plugins/getmdl-select/material.min.js"></script>
<script defer src="<?= base_url() ?>assets/plugins/getmdl-select/getmdl-select.min.js"></script>
<!-- end js include path -->
<script>
    var gst = <?= $this->config->item('gst') ?>;
    var services = <?= json_encode($services) ?>;
    $("#sample6").on('change', function () {
        var service_id = $("#sample6_val").val();

        $("#proxy_service").val(service_id);
        services.map(function (s) {
            if (s.id == service_id) {
                $("#service_price").val(s.service_price);
                calculateTotal();
            }
        })
    });
    $("#sample7").on('change', function(){
        var service_id = $("#sample7val").val();
        console.log(service_id)
        if(service_id=="-1"){
            createNewCategory();
        }
    });
    $("#list3").on('change', function () {
        var room_id = $("#list3").attr('data-val');
        $("#proxy_room").val(room_id);
    });
    $("#service_price").on('input propertychange paste', function () {
        calculateTotal();
    });
    $("#tax").on('input propertychange paste', function () {
        calculateTotal();
    });

    function calculateTotal() {
        var price = parseInt($("#service_price").val());
        var tax = parseInt($("#tax").val());
        if (isNaN(tax)) {
            tax = 0;
        }
        if (isNaN(price)) {
            price = 0;
        }
        var total = price + (price * tax / 100);
        $("#total_price").val(total);
    }

    function createNewCategory() {
        var name = prompt('Enter category name');
        $("#sample7list").append('<li class="mdl-menu__item" data-val="' + name + '" >' + name + '</li>')

    }

    getmdlSelect.init($("#roomList"));

</script>
</body>
</html>