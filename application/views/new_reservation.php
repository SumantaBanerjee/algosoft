<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title><?= $this->config->item('hotel_name') ?></title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="<?= base_url() ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!--bootstrap -->
    <link href="<?= base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet" media="screen"/>
    <link href="<?= base_url() ?>/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet"
          media="screen"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/material/material.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/material_style.css"/>
    <link href="<?= base_url() ?>/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css"/>
    <!-- animation -->
    <link href="<?= base_url() ?>/assets/css/pages/animate_page.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.css"/>
    <!-- Template Styles -->
    <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/theme-color.css" rel="stylesheet" type="text/css"/>
    <!-- favicon -->
    <link rel="shortcut icon" href="./assets/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <!-- logo start -->
            <div class="page-logo">
                <a href="<?= base_url() ?>index.php/dashboard">
                    <img alt="" src="./assets/img/logo.png"/>
                    <span class="logo-default"><?= $this->config->item('software_name') ?></span> </a>
            </div>
            <!-- logo end -->
            <ul class="nav navbar-nav navbar-left in">
                <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
            </ul>
            <!-- start mobile menu -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- end mobile menu -->
            <!-- start header menu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle " src="<?= base_url() ?>assets/img/dp.jpg"/>
                            <span class="username username-hide-on-mobile"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default animated fadeIn">
                            <li>
                                <a href="<?= base_url() ?>index.php/profile">
                                    <i class="icon-user"></i> Profile </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/settings">
                                    <i class="icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        <?= $menu_bar ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">New Reservation Details</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                                   href="<?= base_url() ?>index.php/booking/all">Bookings</a>&nbsp;<i
                                        class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">New Reservation</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <div class="row">
                    <form method="post" id="form1">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-lg-6">
                                <div class="card-box">
                                    <div class="card-head">
                                        <header>Guest Details</header>
                                    </div>
                                    <div class="card-body row">
                                        <div class="col-lg-6 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="text" name="guest_name" required/>
                                                <label class="mdl-textfield__label">Guest Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="text" name="phone_number" required/>
                                                <label class="mdl-textfield__label">Phone Number</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="text" name="email_address" required/>
                                                <label class="mdl-textfield__label">Email Address</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="text" name="address" required/>
                                                <label class="mdl-textfield__label">Address</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="number" name="adults"
                                                       value="0" min="0"/>
                                                <label class="mdl-textfield__label">Adults</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="number" name="children"
                                                       value="0" min="0"/>
                                                <label class="mdl-textfield__label">Children</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                                <textarea class="mdl-textfield__input" rows="3"
                                                          name="remarks"></textarea>
                                                <label class="mdl-textfield__label" for="text7">Remarks</label>
                                            </div>
                                        </div>
                                        <!--<div class="col-lg-12 p-t-20 text-center">
                                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                                            <button type="reset" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                                        </div>-->
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                            <div class="col-md-6 col-sm-6 col-lg-6">
                                <div class="card-box">
                                    <div class="card-head">
                                        <header>Boarding Details</header>
                                    </div>
                                    <div class="card-body row">
                                        <div class="col-lg-6 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="date" id="check_in"
                                                       name="check_in" value="<?= date("Y-m-d") ?>"/>
                                                <label class="mdl-textfield__label">Check In date</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="date" id="check_out"
                                                       name="check_out"
                                                       value="<?= date('Y-m-d', strtotime(date("Y-m-d") . ' +1 day')) ?>"/>
                                                <label class="mdl-textfield__label">Check out date</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 p-t-20 text-center">
                                            <button type="button"
                                                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink"
                                                    id="btnCheckAvailability">Check Availability
                                            </button>
                                        </div>
                                        <input name="rooms_list" id="rooms_list" hidden/>
                                        <div id="rooms_container" class="col-lg-12"
                                             style="height:280px; overflow-y: auto">
                                            <div class="row">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-lg-12">
                                <div class="card-box">
                                    <div class="card-head">
                                        <header>Summary</header>
                                    </div>
                                    <div class="card-body row">
                                        <div class="col-lg-12 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="text" id="summary_rooms" readonly
                                                       value="No Rooms selected">
                                                <label class="mdl-textfield__label">Selected Rooms</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="number" id="summary_cost" readonly value="0">
                                                <label class="mdl-textfield__label">Base Price</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="number" id="summary_gst" readonly value="0">
                                                <label class="mdl-textfield__label">GST</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 p-t-20">
                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                <input class="mdl-textfield__input" type="number" id="summary_total" readonly value="0">
                                                <label class="mdl-textfield__label">Total Cost</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 p-t-20 text-center">
                            <button type="submit"
                                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">
                                Confirm Booking
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- end page content -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> <?= date('Y') ?> &copy; Developed by
            <a href="#" target="_top" class="makerCss">Algosoft</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- end footer -->
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/popper/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Common js-->
<script src="<?= base_url() ?>/assets/js/app.js"></script>
<script src="<?= base_url() ?>/assets/js/layout.js"></script>
<script src="<?= base_url() ?>/assets/js/theme-color.js"></script>
<!-- Material -->
<script src="<?= base_url() ?>/assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>/assets/js/pages/ui/animations.js"></script>

<!-- Sweet Alert -->
<script src="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>/assets/js/pages/sweet-alert/sweet-alert-data.js"></script>
<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/material_select/getmdl-select.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/datetimepicker.js"></script>
<!-- end js include path -->
<script>
    var gst = <?= $this->config->item('gst') ?>;
    var selectedRooms = [];
    var total_cost = 0;
    $("#btnCheckAvailability").on('click', function () {
        swal({
            title: '<?= $this->config->item('hotel_name') ?>',
            text: 'Checking for rooms',
            showConfirmButton: false
        });
        var check_in = new Date($("#check_in").val()).toLocaleDateString();
        var check_out = new Date($("#check_out").val()).toLocaleDateString();
        check_in = check_in.replace(/\//g, "-");
        check_out = check_out.replace(/\//g, "-");
        $.get('<?= base_url() ?>index.php/rooms/getAvailableRooms/' + check_in + "/" + check_out, function (data) {
            swal.close();
            var rooms = JSON.parse(data);
            $("#rooms_container").html("");
            {
                var element = $("<div>").addClass("row");
                var nameElem = $("<div>").addClass("col-lg-3 p-t-20").html("<b>Room</b>");
                var roomNumberElem = $("<div>").addClass("col-lg-3 p-t-20").html("<b>Room Number</b>");
                var priceElem = $("<div>").addClass("col-lg-3 p-t-20").html("<b>Price</b>");
                var btnElem = $("<div>").addClass("col-lg-3 p-t-20").html("<b>Action</b>");
                nameElem.appendTo(element);
                roomNumberElem.appendTo(element);
                priceElem.appendTo(element);
                btnElem.appendTo(element);
                element.appendTo("#rooms_container");
            }
            rooms.map(function (item, i, arr) {
                var element = $("<div>").addClass("row");
                var nameElem = $("<div>").addClass("col-lg-3 p-t-20").text(item.room_name);
                var roomNumberElem = $("<div>").addClass("col-lg-3 p-t-20").text(item.room_number);
                var priceElem = $("<div>").addClass("col-lg-3 p-t-20").text(item.price);
                var btnElem = $("<div>").addClass("col-lg-3 p-t-20").html(
                    $("<button>").attr("type", "button")
                        .addClass("mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink")
                        .on('click', function () {
                            //console.log('Clicked '+item.id)
                            var room_id = item.id;
                            toogleRoomAddition($(this), item);
                        }).text("Book")
                );
                nameElem.appendTo(element);
                roomNumberElem.appendTo(element);
                priceElem.appendTo(element);
                btnElem.appendTo(element);
                element.appendTo("#rooms_container");
                /*$("<div>").addClass("col-lg-3 p-t-20").text(item.room_number),
                $("<div>").addClass("col-lg-3 p-t-20").text(item.total_price),
                $("<div>").addClass("col-lg-3 p-t-20").html(
                    $("<button>").attr("type","button").addClass("mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink").text("Book")
                )
            );*/


                //.appendTo("#rooms_container");
                console.log(element.html());
            })
        })
    });

    function toogleRoomAddition(btn, room){
        if(selectedRooms.indexOf(room.id)>=0){
            //room exists
            btn.text('BOOK');
            selectedRooms.splice(selectedRooms.indexOf(room.id),1);
            total_cost-=parseInt(room.price);
        }else{
            //new room
            btn.text('BOOKED');
            selectedRooms.push(room.id);
            total_cost+=parseInt(room.price);
        }
        $("#rooms_list").val(selectedRooms);
        var roomsArray=[];
        selectedRooms.map(function(item){
            roomsArray.push("Room no. "+item);
        });
        if(roomsArray.length>=0) {
            $("#summary_rooms").val(roomsArray);
        }else{
            $("#summary_rooms").val("No rooms selected");
        }
        var gstCost = (total_cost*gst)/100;
        var totalWithTax = total_cost + gstCost;
        $("#summary_cost").val(total_cost);
        $("#summary_gst").val(gstCost);
        $("#summary_total").val(totalWithTax);
    }

    $("#form1").on('submit', function(){
        swal({
            title: '<?= $this->config->item('hotel_name') ?>',
            text: 'Please wait',
            showConfirmButton: false
        });
    });

</script>
</body>
</html>