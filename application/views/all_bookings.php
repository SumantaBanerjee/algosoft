<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title><?= $this->config->item('hotel_name') ?></title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="<?= base_url() ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!--bootstrap -->
    <link href="<?= base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/material/material.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/material_style.css"/>
    <link href="<?= base_url() ?>/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css"/>
    <!-- animation -->
    <link href="<?= base_url() ?>/assets/css/pages/animate_page.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.css"/>
    <!-- Template Styles -->
    <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/theme-color.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/linearicon.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/iziModal.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/iziToast.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>assets/plugins/getmdl-select/material.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/plugins/getmdl-select/getmdl-select.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/css-loader.css" rel="stylesheet" type="text/css"/>
    <!-- data tables -->
    <link href="<?= base_url() ?>/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css"
          rel="stylesheet" type="text/css"/>
    <!-- favicon -->
    <link rel="shortcut icon" href="./assets/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <!-- logo start -->
            <div class="page-logo">
                <a href="<?= base_url() ?>index.php/dashboard">
                    <img alt="" src="./assets/img/logo.png"/>
                    <span class="logo-default"><?= $this->config->item('software_name') ?></span> </a>
            </div>
            <!-- logo end -->
            <ul class="nav navbar-nav navbar-left in">
                <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
            </ul>
            <!-- start mobile menu -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- end mobile menu -->
            <!-- start header menu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle " src="<?= base_url() ?>assets/img/dp.jpg"/>
                            <span class="username username-hide-on-mobile"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default animated fadeIn">
                            <li>
                                <a href="<?= base_url() ?>index.php/profile">
                                    <i class="icon-user"></i> Profile </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/settings">
                                    <i class="icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container" ng-app="myApp" ng-controller="myCtrl">
        <!-- start sidebar menu -->
        <?= $menu_bar ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">All Bookings</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><a class="parent-item"
                                   href="<?= base_url() ?>index.php/booking/all">Bookings</a>&nbsp;<i
                                        class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">All Bookings</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-box">
                            <div class="card-head">
                                <header>All Bookings</header>
                                <div class="tools">
                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                </div>
                            </div>
                            <div class="card-body ">
                                <div class="row p-b-20">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="btn-group">
                                            <a href="<?= base_url() ?>index.php/booking/create" id="addRow"
                                               class="btn btn-info">
                                                Add New <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-scrollable">
                                    <table class="table table-hover table-checkable order-column full-width"
                                           id="room_list">
                                        <thead>
                                        <tr>
                                            <th>Ref #</th>
                                            <th>Guest Name</th>
                                            <th>Phone Number</th>
                                            <th>Check in</th>
                                            <th>Check out</th>
                                            <th>Room Numbers</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($bookings as $booking) { ?>
                                            <tr>
                                                <td><?= $booking->id ?></td>
                                                <td><?= $booking->guest_name ?></td>
                                                <td><?= $booking->phone_number ?></td>
                                                <td><?= date('d/m/Y', strtotime($booking->check_in)) ?></td>
                                                <td><?= date('d/m/Y', strtotime($booking->check_out)) ?></td>
                                                <td><?php foreach ($booking->rooms as $room) {
                                                        echo $room->room_number . ', ';
                                                    } ?></td>
                                                <td>
                                                    <?php if ($booking->status == "Unpaid") { ?>
                                                        <span class="label label-sm label-warning">Unpaid </span>
                                                    <?php } else if ($booking->status == "Paid") { ?>
                                                        <span class="label label-sm label-success">Paid </span>
                                                    <?php } else { ?>
                                                        <span class="label label-sm label-success"
                                                              style="background: linear-gradient(45deg,#BABABA,#D3D3D3) !important">Cancelled </span>
                                                    <?php } ?>
                                                </td>
                                                <td class="valigntop">
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs deepPink-bgcolor dropdown-toggle no-margin"
                                                                type="button" data-toggle="dropdown"
                                                                aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-left" role="menu">
                                                            <li>
                                                                <a href="<?= base_url() ?>index.php/booking/view/<?= $booking->id ?>">
                                                                    <span class="lnr lnr-list"></span> Open Details
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <?php
                                                                if ($booking->checkin_time == null) {
                                                                    echo '<a onclick="quickCheckin('.$booking->id.')"><span class="lnr lnr-enter"></span> Quick Check in</a>';
                                                                } else {
                                                                    echo '<a onclick="quickCheckout('.$booking->id.')"><span class="lnr lnr-exit"></span> Quick Check out</a>';
                                                                }
                                                                ?>
                                                            </li>
                                                            <li>
                                                                <a onclick="openInvoice(<?= $booking->id ?>)">
                                                                    <span class="lnr lnr-printer"></span> Generate
                                                                    Invoice
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onclick="openExtrasInvoice(<?= $booking->id ?>)">
                                                                    <span class="lnr lnr-layers"></span> Extras
                                                                    Invoice
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a onclick="openService(<?= $booking->id ?>)">
                                                                    <span class="lnr lnr-phone"></span> Extra Service
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end page content -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> <?= date('Y') ?> &copy; Developed by
            <a href="#" target="_top" class="makerCss">Algosoft</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <div class="loader loader-default"></div>
    <!-- end footer -->
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/popper/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Common js-->
<script src="<?= base_url() ?>/assets/js/app.js"></script>
<script src="<?= base_url() ?>/assets/js/layout.js"></script>
<script src="<?= base_url() ?>/assets/js/theme-color.js"></script>
<!-- Material -->
<script src="<?= base_url() ?>/assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>/assets/js/pages/ui/animations.js"></script>
<!-- Sweet Alert -->
<script src="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>/assets/js/pages/sweet-alert/sweet-alert-data.js"></script>

<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/material_select/getmdl-select.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/datetimepicker.js"></script>
<!-- data tables -->
<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>assets/js/iziModal.min.js"></script>
<script src="<?= base_url() ?>assets/js/iziToast.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/select2/js/select2.js"></script>
<script defer src="<?= base_url() ?>assets/plugins/getmdl-select/material.min.js"></script>
<script defer src="<?= base_url() ?>assets/plugins/getmdl-select/getmdl-select.min.js"></script>
<script src="<?= base_url() ?>assets/js/angular.min.js"></script>
<!-- end js include path -->
<script>
    $("#extras_modal").iziModal();

    let bookings = <?= json_encode($bookings) ?>;

    function getBooking(id) {
        return bookings.filter(e => {
            return e.id == id;
        })[0];
    }

    $("#room_list").dataTable({
        "order": [[0, 'desc']]
    });

    function openInvoice(booking_id) {
        var booking = getBooking(booking_id);
        if(booking.checkin_time==null && booking.checkout_time==null){
            if(booking.status=='Cancelled'){
                iziToast.error({
                    timeout: 2500,
                    title: 'Error',
                    message: 'Cannot generate invoice for a cancelled booking.',
                });
            }else{
                iziToast.warning({
                    timeout: 2500,
                    title: 'Wait',
                    message: 'You need to check out the guest before generating the invoice.',
                });
            }
        }else{
            $(".loader").addClass("is-active");
            window.location = "<?= base_url() ?>index.php/booking/invoice/"+booking_id;
        }
    }
    function openExtrasInvoice(booking_id) {
        var booking = getBooking(booking_id);
        if(booking.checkin_time==null && booking.checkout_time==null){
            if(booking.status=='Cancelled'){
                iziToast.error({
                    timeout: 2500,
                    title: 'Error',
                    message: 'Cannot generate invoice for a cancelled booking.',
                });
            }else{
                iziToast.warning({
                    timeout: 2500,
                    title: 'Wait',
                    message: 'You need to check out the guest before generating the invoice.',
                });
            }
        }else{
            $(".loader").addClass("is-active");
            window.location = "<?= base_url() ?>index.php/booking/serviceinvoice/"+booking_id;
        }
    }

    function openService(booking_id) {
        $(".loader").addClass("is-active");
        window.location = '<?= base_url()?>index.php/services/quick/' + booking_id;
    }

    function quickCheckin(booking_id) {
        var booking = getBooking(booking_id);
        if(booking.status=='Cancelled'){
            iziToast.error({
                timeout: 2500,
                title: 'Error',
                message: 'This booking is cancelled.',
            });
        }else {
            $("#modal").iziModal('open');
            $("#modal").iziModal('startLoading');
            $.post('<?= base_url() ?>index.php/booking/checkin/' + booking_id, function (data) {
                $("#modal").iziModal('stopLoading');
                $("#modal").iziModal('close');
                if (data == 1) {
                    iziToast.success({
                        timeout: 1000,
                        title: 'Check in',
                        message: 'Successfully checked in!',
                        onClosed: function (data) {
                            $(".loader").addClass("is-active");
                            location.reload();
                        }
                    });
                } else {
                    iziToast.error({
                        timeout: 1000,
                        title: 'Error',
                        message: 'Checkin unsuccessful',
                    });
                }
            });
        }
    }
    function quickCheckout(booking_id) {
        var booking = getBooking(booking_id);
        if(booking.status=='Cancelled'){
            iziToast.error({
                timeout: 2500,
                title: 'Error',
                message: 'This booking is cancelled.',
            });
        }else if(booking.checkout_time!=null) {
            iziToast.warning({
                timeout: 2500,
                title: 'Wait',
                message: 'Guest already checked out.',
            });
        }else{
            $("#modal").iziModal('open');
            $("#modal").iziModal('startLoading');
            $.post('<?= base_url() ?>index.php/booking/checkout/' + booking_id, function (data) {
                $("#modal").iziModal('stopLoading');
                $("#modal").iziModal('close');
                if (data == 1) {
                    iziToast.success({
                        timeout: 1000,
                        title: 'Check in',
                        message: 'Successfully checked out!',
                        onClosed: function (data) {
                            $(".loader").addClass("is-active");
                            location.reload();
                        }
                    });
                } else {
                    iziToast.error({
                        timeout: 1000,
                        title: 'Error',
                        message: 'Checkout unsuccessful',
                    });
                }
            });
        }
    }
</script>
<script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function ($scope) {
        $scope.test = 'Hello World';
    });
</script>
</body>
</html>