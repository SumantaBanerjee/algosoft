<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title><?= $this->config->item('hotel_name') ?></title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="<?= base_url() ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!--bootstrap -->
    <link href="<?= base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/material/material.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/material_style.css"/>
    <link href="<?= base_url() ?>/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css"/>
    <!-- animation -->
    <link href="<?= base_url() ?>/assets/css/pages/animate_page.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.css"/>
    <!-- Template Styles -->
    <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/theme-color.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/iziModal.min.css" rel="stylesheet" type="text/css"/>
    <!-- favicon -->
    <link rel="shortcut icon" href="./assets/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        .removed{
            text-decoration: line-through;
        }
    </style>
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <!-- logo start -->
            <div class="page-logo">
                <a href="<?= base_url() ?>index.php/dashboard">
                    <img alt="" src="./assets/img/logo.png"/>
                    <span class="logo-default"><?= $this->config->item('software_name') ?></span> </a>
            </div>
            <!-- logo end -->
            <ul class="nav navbar-nav navbar-left in">
                <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
            </ul>
            <!-- start mobile menu -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- end mobile menu -->
            <!-- start header menu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle " src="<?= base_url() ?>assets/img/dp.jpg"/>
                            <span class="username username-hide-on-mobile"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default animated fadeIn">
                            <li>
                                <a href="<?= base_url() ?>index.php/profile">
                                    <i class="icon-user"></i> Profile </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/settings">
                                    <i class="icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        <?= $menu_bar ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <!-- Modal Room Cancellation -->
        <div class="card-box" id="modalRoomCancellation" style="display: none">
            <div class="card-head">
                <header>Cancellation Fee</header>
            </div>
            <div class="class-body row p-2">
                <div class="col-lg-12 p-t-20">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class="mdl-textfield__input" type="text" id="txtCancellationFee"
                               value="0"/>
                        <label class="mdl-textfield__label">Please enter cancellation fee here</label>
                    </div>
                </div>
                <div class="col-lg-12">
                    <label class="mdl-checkbox mdl-js-checkbox" for="chkCancelEmail">
                        <input type="checkbox" id="chkCancelEmail" class="mdl-checkbox__input" />
                        <span class="mdl-checkbox__label"> Send cancellation email</span>
                    </label>
                </div>
                <div class="col-md-12 text-center p-t-10">
                    <button type="button"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink"
                            onclick="cancelRoom()">
                        Submit
                    </button>
                    <button type="button"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-dark" data-iziModal-close>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
        <!-- Modal Room Cancellation End -->

        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">View Reservation Details</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                                   href="<?= base_url() ?>index.php/booking/all">Bookings</a>&nbsp;<i
                                        class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">View Reservations</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <div class="row">
                    <form method="post" id="service_form">
                        <div class="col-sm-12">
                            <div class="card-box">
                                <div class="card-head">
                                    <header>Booking ID #<?= $booking->id ?></header>
                                </div>
                                <div class="card-body row">
                                    <div class="col-lg-6 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" type="text" id="txtRoomNo" readonly
                                                   value="<?= isset($guest->guest_name) ? $guest->guest_name : '' ?>"/>
                                            <label class="mdl-textfield__label">Guest Name</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" type="number" id="txtRoomNo" readonly
                                                   value="<?= isset($booking->guest_count) ? $booking->guest_count : '' ?>"/>
                                            <label class="mdl-textfield__label">Guest Count</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" type="text"
                                                   pattern="-?[0-9]*(\.[0-9]+)?" id="text3" readonly
                                                   name="service_price"
                                                   value="<?= isset($booking->adult_count) ? $booking->adult_count : '0' ?>"/>
                                            <label class="mdl-textfield__label" for="text3">Adult Count</label>
                                            <span class="mdl-textfield__error">Number required!</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" type="text"
                                                   pattern="-?[0-9]*(\.[0-9]+)?" id="text3" readonly
                                                   name="service_price"
                                                   value="<?= isset($booking->children_count) ? $booking->children_count : '0' ?>"/>
                                            <label class="mdl-textfield__label" for="text3">Children Count</label>
                                            <span class="mdl-textfield__error">Number required!</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" type="text"
                                                   pattern="-?[0-9]*(\.[0-9]+)?" id="text3" readonly
                                                   name="service_price"
                                                   value="<?= isset($guest->phone_number) ? $guest->phone_number : '0' ?>"/>
                                            <label class="mdl-textfield__label" for="text3">Phone Number</label>
                                            <span class="mdl-textfield__error">Number required!</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" type="text"
                                                   pattern="-?[0-9]*(\.[0-9]+)?" id="text3" readonly
                                                   name="service_price"
                                                   value="<?= isset($guest->email_address) ? $guest->email_address : '0' ?>"/>
                                            <label class="mdl-textfield__label" for="text3">Email Address</label>
                                            <span class="mdl-textfield__error">Number required!</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" type="text" id="text3" readonly
                                                   name="service_price"
                                                   value="<?= isset($booking->check_in) ? $booking->check_in : '' ?>"/>
                                            <label class="mdl-textfield__label" for="text3">Check In</label>
                                            <span class="mdl-textfield__error">Number required!</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                            <input class="mdl-textfield__input" type="text" id="text3" readonly
                                                   name="service_price"
                                                   value="<?= isset($booking->check_out) ? $booking->check_out : '' ?>"/>
                                            <label class="mdl-textfield__label" for="text3">Check Out</label>
                                            <span class="mdl-textfield__error">Number required!</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <table class="table table display product-overview mb-30" width="100%">
                                            <thead>
                                            <tr>
                                                <td>Room Name</td>
                                                <td>Room Number</td>
                                                <td align="center">Type</td>
                                                <td>Rent per Night</td>
                                                <td align="center">Action</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($rooms as $room) { ?>
                                                <tr id="reservation1<?= $room->id ?>" <?php if($room->cancelled==1) echo 'class="removed"' ?>>
                                                    <td align="left">
                                                        <span><?= isset($room->room_name) ? $room->room_name : '' ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?= isset($room->room_number) ? $room->room_number : '' ?></span>
                                                    </td>
                                                    <td align="center">
                                                        <span class="label label-sm label-event"><?= isset($room->room_type) ? $room->room_type : '' ?></span>
                                                    </td>
                                                    <td>
                                                        <span><?= isset($room->price) ? $room->price : '' ?></span>
                                                    </td>
                                                    <td align="left">
                                                        <button class="btn btn-xs deepPink-bgcolor dropdown-toggle no-margin" type="button"
                                                                data-toggle="dropdown" aria-expanded="false"
                                                                onclick="removeRoom(<?= $room->id ?>, <?= $room->cancelled ?>)">
                                                            <span id="reservation2<?= $room->id ?>"><?= $room->cancelled==1?'Cancelled':'Cancel Room' ?></span>
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-12 p-t-20 text-center">
                                        <button type="button"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-green" onclick="markPaid()">
                                            <?= $booking->status!='Cancelled'?($booking->status=='Unpaid'?'Mark Paid':'Paid'):'Cancelled' ?>
                                        </button>
                                        <button type="button"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-danger" onclick="cancelBooking()">
                                            <?= $booking->status!='Cancelled'?'Cancel':'Cancelled' ?>
                                        </button>
                                        <a type="button" href="<?= base_url() ?>index.php/services/quick/<?= $booking->id ?>"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-success">
                                            Manage Services
                                        </a>
                                        <button type="submit"
                                                class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end page content -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> <?= date('Y') ?> &copy; Developed by
            <a href="#" target="_top" class="makerCss">Algosoft</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- end footer -->
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/popper/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Common js-->
<script src="<?= base_url() ?>/assets/js/app.js"></script>
<script src="<?= base_url() ?>/assets/js/layout.js"></script>
<script src="<?= base_url() ?>/assets/js/theme-color.js"></script>
<!-- Material -->
<script src="<?= base_url() ?>/assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>/assets/js/pages/ui/animations.js"></script>

<!-- Sweet Alert -->
<script src="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>/assets/js/pages/sweet-alert/sweet-alert-data.js"></script>
<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/material_select/getmdl-select.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/datetimepicker.js"></script>
<script src="<?= base_url() ?>assets/js/iziModal.min.js"></script>
<!-- end js include path -->
<script>
    var cancellableRoom = -1;
    $("#modalRoomCancellation").iziModal({
        width: 400
    });
    console.log('<?= json_encode($rooms) ?>');
    var gst = <?= $this->config->item('gst') ?>;
    var booking_id = <?= $booking->id ?>;
    function removeRoom(room_id, cancelled){
        if(cancelled){
            return;
        }
        cancellableRoom = room_id;
        $("#txtCancellationFee").val(0);
        $("#modalRoomCancellation").iziModal('open');
        console.log('Done')
    }
    function cancelRoom(){
        $("#modalRoomCancellation").iziModal('startLoading');
        console.log('Room_id: '+cancellableRoom);
        $.post('<?= base_url() ?>index.php/booking/cancelreservation/'+cancellableRoom+'/'+$("#txtCancellationFee").val()+'/'+$("#chkCancelEmail").is(':checked'), function(data){
            $("#modalRoomCancellation").iziModal('stopLoading');
            $("#modalRoomCancellation").iziModal('close');
            if(data==1){
                $("#reservation1"+cancellableRoom).addClass('removed');
                $("#reservation2"+cancellableRoom).html('Cancelled');
                /*$("#reservation3"+room_id).remove();
                $("#reservation4"+room_id).remove();*/
                swal('The reservation has been cancelled');
            }

        })
    }
    function markPaid(){
        var status = "<?= $booking->status ?>";
        if(status=="Unpaid"){
            swal({
                    title: "<?= $this->config->item('hotel_name') ?>",
                    text: "Do you want to mark this booking as paid?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Yes, Mark Paid.",
                    closeOnConfirm: false
                },
                function(){
                    $.post('<?= base_url() ?>index.php/booking/markpaid/'+booking_id, function(data){
                        if(data==1){
                            swal('The reservation has been marked paid');
                            location.reload();
                        }
                    })
                });
        }
    }

    function cancelBooking(){
        var status = "<?= $booking->status ?>";
        if(status!="Cancelled"){

            swal({
                    title: "<?= $this->config->item('hotel_name') ?>",
                    text: "Do you want to mark this booking as cancelled?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Yes, Cancel.",
                    closeOnConfirm: false
                },
                function(){
                    $.post('<?= base_url() ?>index.php/booking/cancel/'+booking_id, function(data){
                        if(data==1){
                            swal('The reservation has been marked paid');
                            location.reload();
                        }
                    })
                });
        }
    }
</script>
</body>
</html>