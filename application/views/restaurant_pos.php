<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title><?= $this->config->item('hotel_name') ?></title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="<?= base_url() ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!--bootstrap -->
    <link href="<?= base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/material/material.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/material_style.css"/>
    <link href="<?= base_url() ?>/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
    <!-- animation -->
    <link href="<?= base_url() ?>/assets/css/pages/animate_page.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.css" />
    <!-- Template Styles -->
    <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/theme-color.css" rel="stylesheet" type="text/css"/>
    <!-- data tables -->
    <link href="<?= base_url() ?>/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- favicon -->
    <link rel="shortcut icon" href="./assets/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        .food_item{
            cursor: pointer;
        }
        .food_item:hover{
            background: #90a3c7;
        }
    </style>
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <!-- logo start -->
            <div class="page-logo">
                <a href="<?= base_url() ?>index.php/dashboard">
                    <img alt="" src="./assets/img/logo.png"/>
                    <span class="logo-default"><?= $this->config->item('software_name') ?></span> </a>
            </div>
            <!-- logo end -->
            <ul class="nav navbar-nav navbar-left in">
                <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
            </ul>
            <!-- start mobile menu -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- end mobile menu -->
            <!-- start header menu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle " src="<?= base_url() ?>assets/img/dp.jpg"/>
                            <span class="username username-hide-on-mobile"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default animated fadeIn">
                            <li>
                                <a href="<?= base_url() ?>index.php/profile">
                                    <i class="icon-user"></i> Profile </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/settings">
                                    <i class="icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        <?= $menu_bar ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Restaurant</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><a class="parent-item" href="<?= base_url() ?>index.php/dashboard">Dashboard</a>&nbsp;<i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Restaurant</li>
                        </ol>
                    </div>
                </div>
                <div class="row" ng-app="myApp" ng-controller="myCtrl">
                    <div class="col-md-9">
                        <div class="card card-box" style="height: 500px;">
                            <div class="card-head">
                                <header>Restaurant POS</header>
                                <div class="tools">
                                    <!--<input type="checkbox" ng-model="_veg" ng-change="changeVeg()"> Veg
                                    <input type="checkbox" ng-model="_non" ng-change="changeNonVeg()"> Non veg-->
                                    <input type="search" ng-model = "txtSearch" placeholder="search..."/>
                                </div>
                            </div>
                            <div class="card-body" style="overflow-y: auto" >
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col-lg-3">
                                                <ul class="nav nav-tabs tabs-left">
                                                    <li class="nav-item" ng-repeat="category in categories">
                                                        <a href="#tab_{{category}}" data-toggle="tab"> {{category}} ({{by_category[category].length}})</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-lg-9">
                                                <div class="tab-content" id="room_details">
                                                    <!--<div class="tab-pane active" id="tab_all">
                                                        <div class="demo-grid-ruler mdl-grid">
                                                            <?php /*foreach ($catalog as $item) { */?>
                                                                <div ng-click='addItem(<?/*= json_encode($item) */?>)' class="mdl-gridcell mdl-cell--3-col p-2 food_item"><small><?/*= $item->item_name */?></small></div>
                                                            <?php /*} */?>
                                                        </div>
                                                    </div>-->
                                                    <div class="tab-pane fade" id="tab_{{category}}" ng-repeat="category in categories">
                                                        <div  class="col-md-12 demo-grid-ruler mdl-grid">
                                                            <div ng-click='addItem(item)' class="mdl-gridcell mdl-cell--3-col food_item" ng-repeat="item in by_category[category] | filter: searchFood"><small>{{item.item_name}}</small></div>
                                                        </div>
                                                    </div>
                                                    <?php /*foreach ($categories as $category) { */?><!--
                                                    <div class="tab-pane fade" id="tab_<?/*= $category->category */?>">
                                                        <?php /*$items = $by_category[$category->category]; */?>
                                                        <div ng-click='addItem(<?/*= json_encode($item) */?>)' class="col-md-12 demo-grid-ruler mdl-grid">
                                                        <?php /*foreach ($items as $item) { */?>
                                                            <div class="mdl-gridcell mdl-cell--3-col food_item"><small><?/*= $item->item_name */?></small></div>
                                                        <?php /*} */?>
                                                        </div>
                                                    </div>
                                                    --><?php /*} */?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" id="printarea">
                        <div class="card-box">
                            <div class="card-head">
                                <header>Cart</header>
                            </div>
                            <div class="card-body row">
                                <table width="100%" >
                                    <tr ng-repeat="item in cart" style="border-bottom: 1px dashed grey">
                                        <td><small>{{item.item_name}}</small></td>
                                        <td width="48px"> <a ng-click="reduce(item.id)"><small>-</small></a> {{item.count}} <a ng-click="increase(item.id)"><small>+</small></a></td>
                                        <td>{{item.price * item.count}}</td>
                                    </tr>
                                    <tr style="border-top: dotted 1px grey">
                                        <td colspan="2">
                                            <h5>Subtotal</h5>
                                        </td>
                                        <td>
                                            ₹{{total}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <h6>CGST 9%</h6>
                                        </td>
                                        <td>
                                            ₹{{total_tax/2}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <h6>SGST 9%</h6>
                                        </td>
                                        <td>
                                            ₹{{total_tax/2}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <h5>Total</h5>
                                        </td>
                                        <td>
                                            ₹{{grand_total | number:2}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="center"><button class="btn btn-success" ng-click="printArea()">Payment</button></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end page content -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> <?= date('Y') ?> &copy; Developed by
            <a href="#" target="_top" class="makerCss">Algosoft</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- end footer -->
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/popper/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Common js-->
<script src="<?= base_url() ?>/assets/js/app.js"></script>
<script src="<?= base_url() ?>/assets/js/layout.js"></script>
<script src="<?= base_url() ?>/assets/js/theme-color.js"></script>
<!-- Material -->
<script src="<?= base_url() ?>/assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>/assets/js/pages/ui/animations.js"></script>
<!-- Sweet Alert -->
<script src="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>/assets/js/pages/sweet-alert/sweet-alert-data.js"></script>

<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/material_select/getmdl-select.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/datetimepicker.js"></script>
<!-- data tables -->
<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<!-- end js include path -->
<script>
    $("#room_list").dataTable();
</script>
<script>
    var app = angular.module('myApp', []);
    app.controller('myCtrl', function($scope) {
        $scope.is_veg = 0;
        $scope.is_nonveg = 0;
        $scope._categories = <?= json_encode($categories) ?>;
        $scope.categories = [];
        $scope.catalog = <?= json_encode($catalog) ?>;
        $scope.by_category = <?= json_encode($by_category) ?>;
        $scope._categories.forEach(m=>{
            $scope.categories.push(m.category);
        });
        console.log($scope.catalog);
        console.log($scope.by_category);
        console.log($scope.categories);
        $scope.cart = [];
        $scope.gst = <?= $this->config->item('gst') ?>;
        $scope.total = 0;
        $scope.total_tax = 0;
        $scope.grand_total = 0;
        $scope.txtSearch = "";

        $scope.categories.map(cat=>{
            var cat1 = $scope.by_category[cat];
            cat1.map(c=>{
                c.isveg = c.veg==1;
                c.isnonveg = c.veg==0;
            })
        });

        $scope.calculate = function(){
            $scope.total = 0;
            $scope.total_tax = 0;
            $scope.grand_total = 0;
            for(var i=0;i<$scope.cart.length;i++){
                var item = $scope.cart[i];
                $scope.total += item.price*item.count;
            }
            $scope.total_tax = $scope.total*$scope.gst/100;
            $scope.grand_total = $scope.total + $scope.total_tax;
        };
        $scope.reduce = function(id){
            var bFound = false;
            var toDelete = false;
            for(var i=0;i<$scope.cart.length;i++){
                if($scope.cart[i].id == id){
                    $scope.cart[i].count = $scope.cart[i].count-1;
                    if($scope.cart[i].count<=0){
                        $scope.cart.splice(i,1);
                        break;
                    }
                    bFound = true;
                    break;
                }
            }
            $scope.calculate();
        };
        $scope.increase = function(id) {
            for(var i=0;i<$scope.cart.length;i++){
                if($scope.cart[i].id == id){
                    $scope.cart[i].count = $scope.cart[i].count+1;
                    break;
                }
            }
            $scope.calculate();
        };
        $scope.addItem = function(data){
            var bFound = false;
            for(var i=0;i<$scope.cart.length;i++){
                if($scope.cart[i].id == data.id){
                    $scope.cart[i].count = $scope.cart[i].count+1;
                    bFound = true;
                    break;
                }
            }
            if(!bFound){
                data['count'] = 1;
                $scope.cart.push(data);
            }
            $scope.calculate();
        };
        $scope.changeVeg = function(){
            if($scope.is_veg==1){
                $scope.is_veg = 0;
            }else{
                $scope.is_veg = 1;
            }
        };
        $scope.changeNonVeg = function(){
            if($scope.is_nonveg==1){
                $scope.is_nonveg = 0;
            }else{
                $scope.is_nonveg = 1;
            }
        };
        $scope.searchFood = function(item){
            if($scope.txtSearch == "" || typeof $scope.txtSearch == "undefined"){
                return true;
            }else{
                return item.item_name.toUpperCase().includes($scope.txtSearch.toUpperCase());
            }
        };
        $scope.printArea = function(){
            var printContents = document.getElementById("printarea").innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    });
</script>
<script>
    /*function printArea(){
        var printContents = document.getElementById("printarea").innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }*/
</script>
</body>
</html>