<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>Spice Hotel | Bootstrap 4 Admin Dashboard Template + UI Kit</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link href="<?= base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iconic/css/material-design-iconic-font.min.css" />
    <!-- bootstrap -->
    <link href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/pages/extra_pages.css" />
    <!-- favicon -->
    <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<div class="limiter">
    <div class="container-login100 page-background">
        <div class="wrap-login100">
            <form class="form-404" />
            <span class="login100-form-logo">
						<i class="zmdi zmdi-flower"></i>
					</span>
            <span class="form404-title p-b-34 p-t-27">
						Forbidden
					</span>
            <p class="content-404">You do not have access to this action.</p>
            <div class="container-login100-form-btn">
                <a class="login100-form-btn" href="<?= base_url() ?>">
                    Go to home page
                </a>
            </div>
            <div class="text-center p-t-27">
                <a class="txt1" href="#">
                    Need Help?
                </a>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/extra_pages/login.js"></script>
<!-- end js include path -->
</body>
</html>