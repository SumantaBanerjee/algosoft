<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title><?= $this->config->item('hotel_name') ?></title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="<?= base_url() ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!--bootstrap -->
    <link href="<?= base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/material/material.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/material_style.css"/>
    <link href="<?= base_url() ?>/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css"/>
    <!-- animation -->
    <link href="<?= base_url() ?>/assets/css/pages/animate_page.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.css"/>
    <!-- Template Styles -->
    <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/theme-color.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/linearicon.css" rel="stylesheet" type="text/css"/>
    <!-- data tables -->
    <link href="<?= base_url() ?>/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css"
          rel="stylesheet" type="text/css"/>
    <!-- favicon -->
    <link rel="shortcut icon" href="./assets/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/css/iziToast.min.css"/>

    <style>
        .card-box:hover{
            box-shadow: none;
        }
    </style>
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <!-- logo start -->
            <div class="page-logo">
                <a href="<?= base_url() ?>index.php/dashboard">
                    <img alt="" src="./assets/img/logo.png"/>
                    <span class="logo-default"><?= $this->config->item('software_name') ?></span> </a>
            </div>
            <!-- logo end -->
            <ul class="nav navbar-nav navbar-left in">
                <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
            </ul>
            <!-- start mobile menu -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- end mobile menu -->
            <!-- start header menu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle " src="<?= base_url() ?>assets/img/dp.jpg"/>
                            <span class="username username-hide-on-mobile"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default animated fadeIn">
                            <li>
                                <a href="<?= base_url() ?>index.php/profile">
                                    <i class="icon-user"></i> Profile </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/settings">
                                    <i class="icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        <?= $menu_bar ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">All Rooms</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><a class="parent-item" href="<?= base_url() ?>index.php/rooms/all">Rooms</a>&nbsp;<i
                                        class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">All Rooms</li>
                        </ol>
                    </div>
                </div>
                <!-- hidden form -->
                <form style="display: none" id="room_form" method="post">

                </form>
                <!-- end hidden form -->
                <!-- Modal -->
                <div id="modal" style="display: none;">
                    <div class="card-box">
                        <div class="card-head">
                            <header>Room Details</header>
                            <button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton"
                                onclick="hideModal()">
                                <i class="material-icons">clear</i>
                            </button>
                        </div>
                        <div class="card-body row" id="room-details">
                            <div class="col-md-3 col-sm-3 col-3">
                                <ul class="nav nav-tabs tabs-left">
                                    <li class="nav-item">
                                        <a href="#tab_6_1" data-toggle="tab" class="active"> Details </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#tab_6_2" data-toggle="tab"> Features </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#tab_6_3" data-toggle="tab"> Pricing </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#tab_6_4" data-toggle="tab"> Variations </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9 col-sm-9 col-9">
                                <form class="tab-content" id="room_details">
                                    <div class="tab-pane active" id="tab_6_1">
                                        <input type="hidden" id="room_id" name="id">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                    <label class="mdl-textfield__label">Room Name</label>
                                                    <input class="mdl-textfield__input" type="text" id="txtRoomNBame" name="room_name" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                    <label class="mdl-textfield__label">Room Number</label>
                                                    <input class="mdl-textfield__input" type="text" id="txtRoomNo" name="room_number" readonly/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                                                    <label for="list3" class="mdl-textfield__label">Room Type</label>
                                                    <input class="mdl-textfield__input" type="text" id="list3" value="" name="room_type" readonly="" tabindex="-1" />
                                                    <label for="list3" class="pull-right margin-0">
                                                        <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                                    </label>
                                                    <ul data-mdl-for="list3" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                                        <?php foreach ($this->config->item('room_types') as $type) {?>
                                                            <li class="mdl-menu__item" data-val="<?= $type ?>"><?= $type ?></li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                    <input class="mdl-textfield__input" type="text" id="txtExtension" name="extension"/>
                                                    <label class="mdl-textfield__label">Extension</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                    <label class="mdl-textfield__label">Maximum Person</label>
                                                    <input class="mdl-textfield__input" type="number" id="txtPersonCount" name="max_capacity"/>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                    <textarea class="mdl-textfield__input" type="text" id="description" name="description"></textarea>
                                                    <label class="mdl-textfield__label">Description</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_6_2">
                                        <div class="row p-t-10">
                                            <div class="col-md-6">
                                                Air Condition
                                            </div>
                                            <div class="col-md-6 pull-left">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-ac">
                                                    <input type="checkbox" id="switch-ac" name="features[ac]" value="1" class="mdl-switch__input" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row p-t-10">
                                            <div class="col-md-6">
                                                Parking Facility
                                            </div>
                                            <div class="col-md-6 pull-left">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-parking">
                                                    <input type="checkbox" id="switch-parking" name="features[parking]" value="1" class="mdl-switch__input" checked="" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row p-t-10">
                                            <div class="col-md-6">
                                                WiFi Connectivity
                                            </div>
                                            <div class="col-md-6 pull-left">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-wifi">
                                                    <input type="checkbox" id="switch-wifi" name="features[wifi]" value="1" class="mdl-switch__input" checked="" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row p-t-10">
                                            <div class="col-md-6">
                                                Free Breakfast
                                            </div>
                                            <div class="col-md-6 pull-left">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-breakfast">
                                                    <input type="checkbox" id="switch-breakfast" name="features[tea]" value="1" class="mdl-switch__input" checked="" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row p-t-10">
                                            <div class="col-md-6">
                                                Room Service
                                            </div>
                                            <div class="col-md-6 pull-left">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-room">
                                                    <input type="checkbox" id="switch-room" name="features[room_service]" value="1" class="mdl-switch__input" checked="" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row p-t-10">
                                            <div class="col-md-6">
                                                Laundry Service
                                            </div>
                                            <div class="col-md-6 pull-left">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-laundry">
                                                    <input type="checkbox" id="switch-laundry" name="features[laundry]" value="1" class="mdl-switch__input" checked="" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_6_3">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                    <input class="mdl-textfield__input" type="number" id="txtRent" name="price" value="0"/>
                                                    <label class="mdl-textfield__label">Rent per night</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                    <input class="mdl-textfield__input" type="text" id="rentWithGst" value="0" readonly/>
                                                    <label class="mdl-textfield__label">Rent including GST</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                    <input class="mdl-textfield__input" type="number" id="txtBookingPrice" name="advance_price" value="0"/>
                                                    <label class="mdl-textfield__label">Advance Booking Price</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                    <input class="mdl-textfield__input" type="text" id="txtCancellationPrice" name="cancellation_fee" value="0"/>
                                                    <label class="mdl-textfield__label">Cancellation Price</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_6_4">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="text-left">Room name</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody id="variations">

                                            </tbody>
                                        </table>

                                        <span id="variation" class="text-center">No variation available <a onclick="createVariation()">Create variation</a></span>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-12 p-t-20 text-center">
                                <button type="button" class="btn btn-info btn-xs m-b-10" onclick="submitForm()">Submit</button>
                                <button type="button" data-iziModal-close class="btn btn-danger btn-xs m-b-10">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal end -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-box">
                            <div class="card-head">
                                <header>All Rooms</header>
                                <div class="tools">
                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                </div>
                            </div>
                            <div class="card-body ">
                                <div class="row p-b-20">
                                    <div class="col-md-6 col-sm-6 col-6">
                                        <div class="btn-group">
                                            <a href="<?= base_url() ?>index.php/rooms/add" id="addRow"
                                               class="btn btn-info">
                                                Add New <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-scrollable">
                                    <table class="table table-hover table-checkable order-column full-width"
                                           id="room_list">
                                        <thead>
                                        <tr>
                                            <th>Room #</th>
                                            <th>Room Name</th>
                                            <th>Room Type</th>
                                            <th>Maximum Capacity</th>
                                            <th>Phone Number</th>
                                            <th>Rent</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $count=1; foreach ($rooms as $room) { ?>
                                            <tr>
                                                <td><span class="dotted-ul" onclick="openRoomEditDialog(<?= $room->id ?>)"><?= $room->room_number ?></span></td>
                                                <td title="<?= $room->room_name ?>"><?= mb_strimwidth($room->room_name, 0, 15, '...') ?></td>
                                                <td><?= $room->room_type ?></td>
                                                <td><?= $room->max_capacity ?> Person<?= $room->max_capacity==1?'':'s' ?></td>
                                                <td><?= $room->extension ?></td>
                                                <td><?= $room->price ?></td>
                                                <td><?= $room->status=='maintenance'?'M':'A' ?></td>
                                                <td class="valigntop">
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs deepPink-bgcolor dropdown-toggle no-margin"
                                                                type="button" data-toggle="dropdown"
                                                                aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-left" role="menu">
                                                            <li>
                                                                <a href="<?= base_url() ?>index.php/rooms/add/<?= $room->id ?>">
                                                                    <!--<i class="icon-pencil"></i> Edit room data </a>-->
                                                                    <span class="lnr lnr-pencil"></span> Edit room data
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?= base_url() ?>index.php/rooms/createVariation/<?= $room->id ?>">
                                                                    <span class="lnr lnr-tag"></span> Create variation
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?= base_url() ?>index.php/rooms/photo/<?= $room->id ?>">
                                                                    <span class="lnr lnr-picture"></span> Manage photos
                                                                </a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <a onclick="markInMaintenance(<?= $room->id ?>, <?= $room->status=='maintenance' ?>)">
                                                                    <span class="lnr lnr-warning"></span>
                                                                    <?= $room->status=='maintenance'?'Mark available':'Mark in maintenance ' ?>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?= base_url() ?>index.php/rooms/checkAvailability/<?= $room->id ?>">
                                                                    <span class="lnr lnr-briefcase"></span> View
                                                                    availability </a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <a href="#" onclick="deleteRoom(<?= $room->id ?>)">
                                                                    <span class="lnr lnr-trash"></span> Delete Room
                                                                    <span class="badge badge-danger">!</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end page content -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> <?= date('Y') ?> &copy; Developed by
            <a href="#" target="_top" class="makerCss">Algosoft</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- end footer -->
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/popper/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Common js-->
<script src="<?= base_url() ?>/assets/js/app.js"></script>
<script src="<?= base_url() ?>/assets/js/layout.js"></script>
<script src="<?= base_url() ?>/assets/js/theme-color.js"></script>
<!-- Material -->
<script src="<?= base_url() ?>/assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>/assets/js/pages/ui/animations.js"></script>
<!-- Sweet Alert -->
<script src="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>/assets/js/pages/sweet-alert/sweet-alert-data.js"></script>

<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/material_select/getmdl-select.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/datetimepicker.js"></script>
<!-- data tables -->
<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/js/iziToast.min.js"></script>
<!-- end js include path -->
<script>
    let gst = <?= $this->config->item('gst') ?>;
    var current_room_id = -1;
    $("#room_list").dataTable({
        "order":[[ 0, 'asc' ]]
    });

    $("#modal").iziModal({

    });
    $("#txtRent").on('keyup', function(){
        $("#rentWithGst").val(parseInt($("#txtRent").val())*(100+gst)/100);
    });
    function deleteRoom(id) {
        swal({
                title: "<?= $this->config->item('hotel_name') ?>",
                text: "Do you want to delete this room?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Delete.",
                closeOnConfirm: false
            },
            function () {
                $.post('<?= base_url() ?>index.php/rooms/delete/' + id, function (data) {
                    if (data == 1) {
                        location.reload();
                    }
                })
            });
    }

    function openRoomEditDialog(id){
        openModal(id);
    }

    function createVariation(){
        window.location = "<?= base_url() ?>index.php/rooms/createVariation/"+current_room_id;
    }

    function openModal(roomId){
        current_room_id = roomId;
        $("#modal").iziModal('open');
        $("#modal").iziModal('startLoading');
        $.get('<?= base_url() ?>index.php/rooms/getRoomById/'+roomId, function(data){
            console.log(data);
            let response = JSON.parse(data);
            $("#room_id").val(response.room.id);
            setTxtFieldVal($("[name='room_name']"),response.room.room_name);
            setTxtFieldVal($("[name='room_number']"),response.room.room_number);
            setTxtFieldVal($("[name='room_type']"),response.room.room_type);
            setTxtFieldVal($("[name='extension']"),response.room.extension);
            setTxtFieldVal($("[name='max_capacity']"),response.room.max_capacity);
            setTxtAreaVal($("#description"),response.room.description);
            setCheckboxVal($("#switch-ac"), response.features.ac);
            setCheckboxVal($("#switch-parking"), response.features.parking);
            setCheckboxVal($("#switch-wifi"), response.features.wifi);
            setCheckboxVal($("#switch-breakfast"), response.features.tea);
            setCheckboxVal($("#switch-room"), response.features.room_service);
            setCheckboxVal($("#switch-laundry"), response.features.laundry);
            setTxtFieldVal($("[name='price']"), response.room.price);
            setTxtFieldVal($("[name='advance_price']"), response.room.advance_price);
            setTxtFieldVal($("[name='cancellation_fee']"), response.room.cancellation_fee);
            setTxtFieldVal($("#rentWithGst"), response.room.price*(100+gst)/100);
            $("#variations").html("");
            var i =1;
            response.variations.map(v=>{
                $("#variations").append('<tr><td>'+(i++)+'</td><td>'+v.room_name+'</td><td><a href="<?= base_url() ?>index.php/rooms/add/'+v.id+'">Open</a></td></tr>');
            });
            if(i!=1){
                $("#variation").hide();
            }else{
                $("#variation").show();
            }
            $("#modal").iziModal('stopLoading');
        })
    }
    function setCheckboxVal(field, val){
        if(val==1) {
            field[0].parentNode.MaterialSwitch.on();
        }else{
            field[0].parentNode.MaterialSwitch.off();
        }
    }
    function setTxtFieldVal(field, val){
        field[0].parentElement.MaterialTextfield.change(val);
    }
    function setTxtAreaVal(field, val){
        try{
            field[0].parentNode.MaterialTextfield.change(val);
        }catch (e){}
    }
    function hideModal(){
        $("#modal").iziModal('close');
    }
    function markInMaintenance(room, is_m){
        iziToast.question({
            timeout: 6000,
            close: false,
            overlay: true,
            toastOnce: true,
            id: 'question',
            zindex: 999,
            title: 'Hey',
            message: (is_m==1)?'Do you want to mark this room as available?':'Do you really want to mark this room in maintenance?',
            position: 'center',
            buttons: [
                ['<button><b>YES</b></button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'yes');

                }, true],
                ['<button>NO</button>', function (instance, toast) {

                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'no');

                }],
            ],
            onClosing: function(instance, toast, closedBy){
                //console.info('Closing | closedBy: ' + closedBy);
            },
            onClosed: function(instance, toast, closedBy){
                if(closedBy=="yes"){
                    window.location = "<?= base_url() ?>index.php/rooms/toggleMaintenance/"+room;
                }else{

                }
            }
        });

    }
    function submitForm(){
        $("#modal").iziModal('startLoading');
        $.post('<?= base_url() ?>index.php/rooms/add', $('#room_details').serialize(), function(data){
            $("#modal").iziModal('stopLoading');
            if(data!=0){
                iziToast.success({
                    title: 'Success',
                    message: 'Room information updated',
                    timeout:1000,
                    position: 'topCenter',
                    onClosed: function(instance, toast, closedBy){
                        location.reload();
                    }
                });
            }else{
                iziToast.error({
                    title: 'Error',
                    message: 'Room information could not be updated',
                    position: 'topCenter'
                });
            }
        })
    }

</script>
</body>
</html>