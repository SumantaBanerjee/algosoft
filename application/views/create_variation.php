<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title><?= $this->config->item('hotel_name') ?></title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="<?= base_url() ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!--bootstrap -->
    <link href="<?= base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/material/material.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/material_style.css"/>
    <link href="<?= base_url() ?>/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
    <!-- animation -->
    <link href="<?= base_url() ?>/assets/css/pages/animate_page.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.css" />
    <!-- Template Styles -->
    <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/theme-color.css" rel="stylesheet" type="text/css"/>
    <!-- favicon -->
    <link rel="shortcut icon" href="./assets/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <!-- logo start -->
            <div class="page-logo">
                <a href="<?= base_url() ?>index.php/dashboard">
                    <img alt="" src="./assets/img/logo.png"/>
                    <span class="logo-default"><?= $this->config->item('software_name') ?></span> </a>
            </div>
            <!-- logo end -->
            <ul class="nav navbar-nav navbar-left in">
                <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
            </ul>
            <!-- start mobile menu -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- end mobile menu -->
            <!-- start header menu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle " src="<?= base_url() ?>assets/img/dp.jpg"/>
                            <span class="username username-hide-on-mobile"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default animated fadeIn">
                            <li>
                                <a href="<?= base_url() ?>index.php/profile">
                                    <i class="icon-user"></i> Profile </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/settings">
                                    <i class="icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        <?= $menu_bar ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title"><?= isset($room)?'Edit':'Add' ?> Room Details</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><a class="parent-item" href="<?= base_url() ?>index.php/rooms/all">Rooms</a>&nbsp;<i
                                    class="fa fa-angle-right"></i>
                            </li>
                            <li class="active"><?= isset($room)?'Edit':'Add' ?> Room</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <div class="row">
                    <form method="post" id="room_form">
                        <div class="col-sm-12">
                            <div class="card-box">
                            <div class="card-head">
                                <header>Enter Room Details</header>
                            </div>
                            <div class="card-body row">
                                <div class="col-lg-12 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text" id="txtRoomName" name="room_name" value="<?= isset($room->room_name)?$room->room_name:'' ?>" />
                                        <label class="mdl-textfield__label">Room Name</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text" id="txtRoomNo" name="room_number" readonly value="<?= isset($room->room_number)?$room->room_number:'' ?>" />
                                        <label class="mdl-textfield__label">Room Number</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                                        <input class="mdl-textfield__input" type="text" id="list3" readonly="" tabindex="-1" value="<?= isset($room->room_type)?$room->room_type:'' ?>" name="room_type"/>
                                        <label for="list3" class="pull-right margin-0">
                                            <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                        </label>
                                        <label for="list3" class="mdl-textfield__label">Room Type</label>
                                        <ul data-mdl-for="list3" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                            <?php foreach ($this->config->item('room_types') as $type) {?>
                                                <li class="mdl-menu__item" data-val="<?= $type ?>"><?= $type ?></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-4 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="text3" name="advance_price" value="<?= isset($room->advance_price)?$room->advance_price:'' ?>"/>
                                        <label class="mdl-textfield__label" for="text3">Advance booking price</label>
                                        <span class="mdl-textfield__error">Number required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-4 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="text7" name="price" value="<?= isset($room->price)?$room->price:'' ?>"/>
                                        <label class="mdl-textfield__label" for="text7">Rent Per Night</label>
                                        <span class="mdl-textfield__error">Number required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-4 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="text7" name="cancellation_fee" value="<?= isset($room->cancellation_fee)?$room->cancellation_fee:'' ?>"/>
                                        <label class="mdl-textfield__label" for="text7">Cancellation fee</label>
                                        <span class="mdl-textfield__error">Number required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="text8" name="extension" value="<?= isset($room->extension)?$room->extension:'' ?>"/>
                                        <label class="mdl-textfield__label" for="text8">Telephone Number</label>
                                        <span class="mdl-textfield__error">Number required!</span>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">
                                        <input class="mdl-textfield__input" type="text" id="list2" value="<?= isset($room->max_capacity)?$room->max_capacity:'' ?>" readonly="" tabindex="-1" name="max_capacity"/>
                                        <label for="list2" class="pull-right margin-0">
                                            <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
                                        </label>
                                        <label for="list2" class="mdl-textfield__label">Bed Capacity</label>
                                        <ul data-mdl-for="list2" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                                            <li class="mdl-menu__item" data-val="1">1</li>
                                            <li class="mdl-menu__item" data-val="2">2</li>
                                            <li class="mdl-menu__item" data-val="3">3</li>
                                            <li class="mdl-menu__item" data-val="4">4</li>
                                            <li class="mdl-menu__item" data-val="5">5</li>
                                            <li class="mdl-menu__item" data-val="6">6</li>
                                            <li class="mdl-menu__item" data-val="7">7</li>
                                            <li class="mdl-menu__item" data-val="8">8</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-2 p-t-20">
                                    <div class="checkbox checkbox-icon-black">
                                        <input id="checkbox1" type="checkbox" name="features[ac]" value="1" <?= (isset($features['ac']) && $features['ac']==1)?"checked":'' ?> />
                                        <label for="checkbox1">
                                            AC
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 p-t-20">
                                    <div class="checkbox checkbox-icon-black">
                                        <input id="checkbox2" type="checkbox" name="features[parking]" value="1" <?= (isset($features['parking']) && $features['parking']==1)?"checked":'' ?>/>
                                        <label for="checkbox2">
                                            Parking
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 p-t-20">
                                    <div class="checkbox checkbox-icon-black">
                                        <input id="checkbox3" type="checkbox" name="features[wifi]" value="1" <?= (isset($features['wifi']) && $features['wifi']==1)?"checked":'' ?>/>
                                        <label for="checkbox3">
                                            Wifi
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 p-t-20">
                                    <div class="checkbox checkbox-icon-black">
                                        <input id="checkbox4" type="checkbox" name="features[tea]" value="1" <?= (isset($features['tea']) && $features['tea']==1)?"checked":'' ?>/>
                                        <label for="checkbox4">
                                            Free Breakfast
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 p-t-20">
                                    <div class="checkbox checkbox-icon-black">
                                        <input id="checkbox5" type="checkbox" name="features[room_service]" value="1" <?= (isset($features['room_service']) && $features['room_service']==1)?"checked":'' ?>/>
                                        <label for="checkbox5">
                                            Room Service
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-2 p-t-20">
                                    <div class="checkbox checkbox-icon-black">
                                        <input id="checkbox6" type="checkbox" name="features[laundry]" <?= (isset($features['laundry']) && $features['laundry']==1)?"checked":'' ?>/>
                                        <label for="checkbox6">
                                            Laundry
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-12 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                        <textarea class="mdl-textfield__input" rows="4" id="education" name="description"><?= isset($room->description)?$room->description:'' ?></textarea>
                                        <label class="mdl-textfield__label" for="text7">Room Details</label>
                                    </div>
                                </div>
                                <div class="col-lg-12 p-t-20 text-center">
                                    <button id="btnSubmit" type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                                    <button id="btnReset" type="reset" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- end page content -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> <?= date('Y') ?> &copy; Developed by
            <a href="#" target="_top" class="makerCss">Algosoft</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- end footer -->
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/popper/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Common js-->
<script src="<?= base_url() ?>/assets/js/app.js"></script>
<script src="<?= base_url() ?>/assets/js/layout.js"></script>
<script src="<?= base_url() ?>/assets/js/theme-color.js"></script>
<!-- Material -->
<script src="<?= base_url() ?>/assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>/assets/js/pages/ui/animations.js"></script>
<!-- Sweet Alert -->
<script src="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>/assets/js/pages/sweet-alert/sweet-alert-data.js"></script>

<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/material_select/getmdl-select.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/datetimepicker.js"></script>
<!-- end js include path -->
<script>

</script>
<script>
    $("#room_form").on('submit', function(e){
        $("#btnSubmit").attr("disabled","disabled");
        $("#btnReset").attr("disabled","disabled");
        e.preventDefault();
        swal({
            title: '<?= $this->config->item('hotel_name') ?>',
            text: "Please wait.",
            imageUrl: "../../app/app-img/loading_spinner.gif",
            showConfirmButton: false
        });
        $.post('<?= base_url() ?>index.php/rooms/add', $('#room_form').serialize(), function(data){
            console.log(data);
            swal.close();
            setTimeout(function() {
                if (data != 0) {
                    swal({
                        title: '<?= $this->config->item('hotel_name') ?>',
                        text: 'The room has been successfully registered!',
                        closeOnConfirm: false,
                        type: 'success'
                    }, function () {
                        window.location = '<?= base_url() ?>index.php/rooms/all';
                    });
                } else {
                    swal({
                        title: '<?= $this->config->item('hotel_name') ?>',
                        text: "Sorry, there was an internal server error!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: true
                    });
                }
                $("#btnSubmit").removeAttr("disabled");
                $("#btnReset").removeAttr("disabled");
            },500);
        })
    })
</script>
</body>
</html>