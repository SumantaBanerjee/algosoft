<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title><?= $this->config->item('hotel_name') ?></title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="<?= base_url() ?>assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!--bootstrap -->
    <link href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/material/material.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/material_style.css"/>
    <!-- animation -->
    <link href="<?= base_url() ?>assets/css/pages/animate_page.css" rel="stylesheet"/>
    <!-- Template Styles -->
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/theme-color.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/iziModal.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>assets/css/iziToast.min.css" rel="stylesheet" type="text/css"/>
    <!-- favicon -->
    <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        .booking{
            cursor: pointer;
        }
        .booking:hover{
            background: #e9e9e9;
        }
        .booking:active{
            background: #cfcfcf;
        }
    </style>
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark page-footer-fixed">
<div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <!-- logo start -->
            <div class="page-logo">
                <a href="<?= base_url() ?>index.php/dashboard">
                    <img alt="" src="<?= base_url() ?>assets/img/logo.png"/>
                    <span class="logo-default"><?= $this->config->item('software_name') ?></span> </a>
            </div>
            <!-- logo end -->
            <ul class="nav navbar-nav navbar-left in">
                <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
            </ul>
            <form class="search-form-opened" action="#" method="GET"/>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search..." name="query"/>
                <span class="input-group-btn search-btn">
                          <a href="javascript:;" class="btn submit">
                             <i class="icon-magnifier"></i>
                           </a>
                        </span>
            </div>
            </form>

            <!-- start mobile menu -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- end mobile menu -->
            <!-- start header menu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle " src="<?= base_url() ?>assets/img/dp.jpg"/>
                            <span class="username username-hide-on-mobile"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default animated jello">
                            <li>
                                <a href="<?= base_url() ?>index.php/profile">
                                    <i class="icon-user"></i> Profile </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/settings">
                                    <i class="icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        <?= $menu_bar ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Dashboard</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                                   href="<?= base_url() ?>index.html">Home</a>&nbsp;<i
                                        class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <div class="state-overview">
                    <div class="row">
                        <div class="col-xl-3 col-md-6 col-12">
                            <div class="info-box bg-blue">
                                <span class="info-box-icon push-bottom"><i class="material-icons">style</i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Total Bookings</span>
                                    <span class="info-box-number"><?= $total_booking_count ?></span>
                                    <div class="progress">
                                        <div class="progress-bar width-60"></div>
                                    </div>
                                    <span class="progress-description">
					                  </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-xl-3 col-md-6 col-12">
                            <div class="info-box bg-orange">
                                <span class="info-box-icon push-bottom"><i class="material-icons">card_travel</i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Future Bookings</span>
                                    <span class="info-box-number"><?= $future_booking_count ?></span>
                                    <div class="progress">
                                        <div class="progress-bar width-40"></div>
                                    </div>
                                    <span class="progress-description">
					                  </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-xl-3 col-md-6 col-12">
                            <div class="info-box bg-purple">
                                <span class="info-box-icon push-bottom"><i
                                            class="material-icons">phone_in_talk</i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Current Bookings</span>
                                    <span class="info-box-number"><?= $current_booking_count ?></span>
                                    <div class="progress">
                                        <div class="progress-bar width-80"></div>
                                    </div>
                                    <span class="progress-description">
					                  </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-xl-3 col-md-6 col-12">
                            <div class="info-box bg-success">
                                <span class="info-box-icon push-bottom"><i
                                            class="material-icons">local_laundry_service</i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Active Services</span>
                                    <span class="info-box-number"><?= sizeof($services) ?></span>
                                    <div class="progress">
                                        <div class="progress-bar width-60"></div>
                                    </div>
                                    <span class="progress-description">
					                  </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-box">
                            <div class="card-head">
                                <header>Quick Revenue</header>
                                <div class="tools">
                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                </div>
                            </div>
                            <div class="card-body no-padding height-9">
                                <div class="row text-center">
                                    <div class="col-sm-3 col-6">
                                        <h4 class="margin-0">₹<?= $today_revenue + $service_rev_t ?> </h4>
                                        <p class="text-muted"> Today's Revenue</p>
                                    </div>
                                    <div class="col-sm-3 col-6">
                                        <h4 class="margin-0">₹<?= $week_revenue + $service_rev_w ?> </h4>
                                        <p class="text-muted">This Week's Revenue</p>
                                    </div>
                                    <div class="col-sm-3 col-6">
                                        <h4 class="margin-0">₹<?= $month_revenue + $service_rev_m ?> </h4>
                                        <p class="text-muted">This Month's Revenue</p>
                                    </div>
                                    <div class="col-sm-3 col-6">
                                        <h4 class="margin-0">₹<?= $year_revenue + $service_rev_y ?> </h4>
                                        <p class="text-muted">This Year's Revenue</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="area_line_chart" class="width-100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-3">
                        <div class="card bg-info">
                            <div class="text-white py-3 px-4">
                                <h6 class="card-title text-white mb-0">Bookings</h6>
                                <p></p>
                                <div id="sparkline26"></div>
                                <small class="text-white">View Details</small>
                            </div>
                        </div>
                        <div class="card bg-success">
                            <div class="text-white py-3 px-4">
                                <h6 class="card-title text-white mb-0">Services</h6>
                                <p></p>
                                <div id="sparkline27"></div>
                                <small class="text-white">View Details</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-4">
                        <div class="card  card-box">
                            <div class="card-head">
                                <header>Service Orders</header>
                                <div class="tools">
                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                </div>
                            </div>
                            <div class="card-body no-padding" style="max-height: 350px; overflow-y: auto">
                                <div class="row">
                                    <div class="noti-information notification-menu">
                                        <div class="notification-list mail-list not-list small-slimscroll-style">
                                            <?php $count=0; foreach ($services as $service) { ?>
                                                <?php if($service->status!='pending') continue; else{$count++;}?>

                                                <a href="<?= base_url() ?>index.php/services/view_qs/<?= $service->id ?>"
                                                   class="single-mail"> <span class="icon bg-primary"> <i
                                                                class="fa fa-user-o"></i>
												</span> <span
                                                            class="text-purple">Room <?= $service->room_number ?></span>
                                                    Asked for <?= $service->service_name ?>
                                                    <span class="notificationtime">
                                                        <small><?= $service->date ?></small>
                                                    </span>
                                                </a>
                                            <?php } ?>
                                            <?php if($count==0) { ?>
                                                <a href="#"
                                                   class="single-mail"> <span class="icon bg-primary"> <i
                                                                class="fa fa-user-o"></i>
												</span> <span
                                                            class="text-purple">Great!</span>
                                                    All requests have been served
                                                    <span class="notificationtime">
                                                        <small>Ready to take new orders</small>
                                                    </span>
                                                </a>
                                            <?php } ?>
                                        </div>
                                        <div class="full-width text-center p-t-10">
                                            <a class="btn purple btn-outline btn-circle margin-0"
                                               href="<?= base_url() ?>index.php/services/quick_services">View All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5 col-5">
                        <div class="card  card-box">
                            <div class="card-head">
                                <header>Today's Booking</header>
                                <div class="tools">
                                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                </div>
                            </div>
                            <div class="card-body no-padding" style="max-height: 350px; overflow-y: auto">
                                <?php foreach ($bookings as $booking) { ?>
                                    <div class="dropdown show booking" id="booking<?= $booking->id ?>">
                                        <div class="row p-2 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="dropdownMenuLink" style="border-bottom: 1px solid #dddddd">
                                            <div class="col-md-1 pull-left" style="display: flex;align-items: center;">
                                                <span>#<?= $booking->id ?></span>
                                            </div>
                                            <div class="col-md-3 text-center">
                                                <p><small>Check In</small></p>
                                                <?= date('M d', strtotime($booking->check_in)) ?>
                                            </div>
                                            <div class="col-md-3">
                                                <p><small>Check Out</small></p>
                                                <?= date('M d', strtotime($booking->check_out)) ?>
                                            </div>
                                            <div class="col-md-4">
                                                <p><small>Customer</small></p>
                                                <small><?= $booking->guest_name ?></small>
                                            </div>
                                            <div class="col-md-1" style="display: flex;align-items: center;">
                                                <i class="fa fa-angle-right" style="font-size:1.5em;"></i>
                                            </div>
                                        </div>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" onclick="quickCheckin(<?= $booking->id ?>)">Quick Checkin</a>
                                            <a class="dropdown-item" href="<?= base_url() ?>index.php/booking/view/<?= $booking->id ?>">View Booking</a>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if(sizeof($bookings)<=0) { ?>
                                    <div class="col-md-12 text-center">No pending booking today</div>
                                <?php } ?>
                                <div class="row">
                                    <div id="donut_chart" class="width-100 height-250"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modal">
                <h3>Please wait</h3>
            </div>
        </div>
        <!-- end page content -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> <?= date('Y') ?> &copy; Developed by
            <a href="#" target="_top" class="makerCss">Algosoft</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- end footer -->
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/popper/popper.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Common js-->
<script src="<?= base_url() ?>assets/js/app.js"></script>
<script src="<?= base_url() ?>assets/js/layout.js"></script>
<script src="<?= base_url() ?>assets/js/theme-color.js"></script>
<!-- Material -->
<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>assets/js/pages/ui/animations.js"></script>


<script src="<?= base_url() ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/sparkline/sparkline-data.js"></script>
<!-- material -->
<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>assets/js/pages/ui/animations.js"></script>
<!-- morris chart -->
<script src="<?= base_url() ?>assets/plugins/morris/morris.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/morris/raphael-min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/chart/morris/morris_home_data.js"></script>
<script src="<?= base_url() ?>assets/js/iziModal.min.js"></script>
<script src="<?= base_url() ?>assets/js/iziToast.min.js"></script>
<!-- end js include path -->

<script>
    $("#modal").iziModal({
        overlayClose: false
    });
    function quickCheckin(booking){
        $("#modal").iziModal('open');
        $("#modal").iziModal('startLoading');
        $.post('<?= base_url() ?>index.php/booking/checkin/'+booking, function(data){
            $("#modal").iziModal('stopLoading');
            $("#modal").iziModal('close');
            if(data==1){
                iziToast.success({
                    timeout: 1000,
                    title: 'Check in',
                    message: 'Successfully checked in!',
                    onClosed: function(data){
                        $(`#booking${booking}`).remove();
                    }
                });
            }else{
                iziToast.error({
                    timeout: 1000,
                    title: 'Error',
                    message: 'Checkout unsuccessful',
                });
            }
        });
    }
</script>
</body>
</html>