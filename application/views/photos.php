<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title><?= $this->config->item('hotel_name') ?></title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="<?= base_url() ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!--bootstrap -->
    <link href="<?= base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/material/material.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/material_style.css"/>
    <link href="<?= base_url() ?>/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
    <!-- animation -->
    <link href="<?= base_url() ?>/assets/css/pages/animate_page.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.css" />
    <!-- Template Styles -->
    <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/theme-color.css" rel="stylesheet" type="text/css"/>


    <!--<link href="<?/*= base_url() */?>/assets/plugins/dropzone/dropzone.css" rel="stylesheet" media="screen" />-->

    <script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
    <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">

    <!-- favicon -->
    <link rel="shortcut icon" href="./assets/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <!-- logo start -->
            <div class="page-logo">
                <a href="<?= base_url() ?>index.php/dashboard">
                    <img alt="" src="./assets/img/logo.png"/>
                    <span class="logo-default"><?= $this->config->item('software_name') ?></span> </a>
            </div>
            <!-- logo end -->
            <ul class="nav navbar-nav navbar-left in">
                <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
            </ul>
            <!-- start mobile menu -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- end mobile menu -->
            <!-- start header menu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle " src="<?= base_url() ?>assets/img/dp.jpg"/>
                            <span class="username username-hide-on-mobile"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default animated fadeIn">
                            <li>
                                <a href="<?= base_url() ?>index.php/profile">
                                    <i class="icon-user"></i> Profile </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/settings">
                                    <i class="icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        <?= $menu_bar ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Upload Room Photos</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><a class="parent-item" href="<?= base_url() ?>index.php/rooms/all">Rooms</a>&nbsp;<i
                                    class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Photos</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="card-head">
                                <header>
                                    <header>Upload photos for Room <b><?= $room->room_number ?></b></header>
                                </header>
                            </div>
                            <div class="card-body row">
                                <div class="col-lg-12 p-t-20">
                                    <label class="control-label col-md-3">Upload Room Photos</label>
                                    <div class="row">
                                        <div class="row text-center text-lg-left">
                                            <?php foreach ($photos as $photo) {?>
                                                <div class="col-lg-3 col-md-4 col-xs-6">
                                                    <a href="<?= $photo->image_url ?>" target="_blank" class="d-block mb-4 h-100">
                                                        <img class="img-fluid img-thumbnail" src="<?= $photo->image_url ?>" alt="">
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <form method="post" action="<?= base_url() ?>index.php/uploader/upload" enctype="multipart/form-data" class="dropzone" id="myAwesomeDropzone">
                                        <input value="<?= $room->id ?>" name="room_id" hidden>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12 text-center"><br/>
                                            <button class="btn btn-success text-center" type="button" id="submit_dropzone_form">UPLOAD</button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page content -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> <?= date('Y') ?> &copy; Developed by
            <a href="#" target="_top" class="makerCss">Algosoft</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- end footer -->
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/popper/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Common js-->
<script src="<?= base_url() ?>/assets/js/app.js"></script>
<script src="<?= base_url() ?>/assets/js/layout.js"></script>
<script src="<?= base_url() ?>/assets/js/theme-color.js"></script>
<!-- Material -->
<script src="<?= base_url() ?>/assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>/assets/js/pages/ui/animations.js"></script>
<!-- Sweet Alert -->
<script src="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>/assets/js/pages/sweet-alert/sweet-alert-data.js"></script>

<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/material_select/getmdl-select.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/datetimepicker.js"></script>


<!-- dropzone -->
<!--<script src="<?/*= base_url() */?>assets/plugins/dropzone/dropzone.js"></script>
<script src="<?/*= base_url() */?>assets/plugins/dropzone/dropzone-call.js"></script>-->
<!-- end js include path -->
<script>
    Dropzone.options.myAwesomeDropzone = {
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads:10,
        successmultiple:function(data,response){
            swal({
                    title: "<?= $this->config->item('hotel_name') ?>",
                    text: "Images uploaded successfully",
                    type: "info",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                },
                function(){
                    location.reload()
                });
        },
        init: function() {
            //Submitting the form on button click
            var submitButton = document.querySelector("#submit_dropzone_form");
            myDropzone = this; // closure
            submitButton.addEventListener("click", function() {
                myDropzone.processQueue(); // Tell Dropzone to process all queued files.
            });
        }
    };
</script>
</body>
</html>