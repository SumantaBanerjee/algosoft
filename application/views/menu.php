<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu">
        <div id="remove-scroll">
            <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="true" data-auto-scroll="false"
                data-slide-speed="200" style="height: 1200px">
                <li class="sidebar-toggler-wrapper hide">
                    <div class="sidebar-toggler">
                        <span></span>
                    </div>
                </li>
                <li class="sidebar-user-panel">
                    <div class="user-panel">
                        <div class="row">
                            <div class="sidebar-userpic">
                                <img src="<?= base_url() ?>assets/img/dp.jpg" class="img-responsive" alt=""/>
                            </div>
                        </div>
                        <div class="profile-usertitle">
                            <div class="sidebar-userpic-name"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </div>
                            <div class="profile-usertitle-job"> <?= isset($_SESSION['user_role']) ? $_SESSION['user_role'] : 'Manager' ?> </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item start">
                    <a href="<?= base_url() ?>index.php/dashboard" class="nav-link nav-toggle">
                        <i class="material-icons">dashboard</i>
                        <span class="title">Dashboard</span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">business_center</i>
                        <span class="title">Booking</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="<?= base_url() ?>index.php/booking/create" class="nav-link ">
                                <span class="title">New Booking</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url() ?>index.php/booking/all" class="nav-link ">
                                <span class="title">View Booking</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">vpn_key</i>
                        <span class="title">Rooms</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="<?= base_url() ?>index.php/rooms/add" class="nav-link ">
                                <span class="title">Add Room Details</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url() ?>index.php/rooms/all" class="nav-link ">
                                <span class="title">View All Rooms</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url() ?>index.php/rooms/status" class="nav-link ">
                                <span class="title">View Available Rooms</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--<li class="nav-item">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">group</i>
                        <span class="title">Guests</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="<?/*= base_url() */?>index.php/guests/all" class="nav-link ">
                                <span class="title">View All Guests</span>
                            </a>
                        </li>
                    </ul>
                </li>-->
                <li class="nav-item">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">directions_run</i>
                        <span class="title">Services</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="<?= base_url() ?>index.php/services/add" class="nav-link ">
                                <span class="title">Add Service Details</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url() ?>index.php/services/all" class="nav-link ">
                                <span class="title">View All Services</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">people</i>
                        <span class="title">Users</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="<?= base_url() ?>index.php/users/add" class="nav-link ">
                                <span class="title">Add User Details</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url() ?>index.php/users/all" class="nav-link ">
                                <span class="title">View All Users</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">local_dining</i>
                        <span class="title">Restaurant</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a target="_blank" href="<?= base_url() ?>index.php/restaurant/" class="nav-link ">
                                <span class="title">Open Restaurant</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>