<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="description" content="Responsive Admin Template"/>
    <meta name="author" content="SmartUniversity"/>
    <title><?= $this->config->item('hotel_name') ?></title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css"/>
    <!-- icons -->
    <link href="<?= base_url() ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url() ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <!--bootstrap -->
    <link href="<?= base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/material/material.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/material_style.css"/>
    <link href="<?= base_url() ?>/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
    <!-- animation -->
    <link href="<?= base_url() ?>/assets/css/pages/animate_page.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.css" />
    <!-- Template Styles -->
    <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url() ?>/assets/css/theme-color.css" rel="stylesheet" type="text/css"/>
    <!-- favicon -->
    <link rel="shortcut icon" href="./assets/img/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @media print
        {
            .no-print, .no-print *
            {
                display: none !important;
            }
        }
    </style>
</head>
<!-- END HEAD -->
<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
<div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <!-- logo start -->
            <div class="page-logo">
                <a href="<?= base_url() ?>index.php/dashboard">
                    <img alt="" src="./assets/img/logo.png"/>
                    <span class="logo-default"><?= $this->config->item('software_name') ?></span> </a>
            </div>
            <!-- logo end -->
            <ul class="nav navbar-nav navbar-left in">
                <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
            </ul>
            <!-- start mobile menu -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- end mobile menu -->
            <!-- start header menu -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle " src="<?= base_url() ?>assets/img/dp.jpg"/>
                            <span class="username username-hide-on-mobile"> <?= isset($_SESSION['user_name']) ? $_SESSION['user_name'] : 'John Doe' ?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default animated fadeIn">
                            <li>
                                <a href="<?= base_url() ?>index.php/profile">
                                    <i class="icon-user"></i> Profile </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/settings">
                                    <i class="icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>index.php/logout">
                                    <i class="icon-logout"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        <?= $menu_bar ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Invoice</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?= base_url() ?>index.php/booking/all">Bookings</a>&nbsp;<i
                                    class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Invoice</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3><b>INVOICE</b> <span class="pull-right">#<?= date('Ymd', strtotime($booking['booking']->check_out)).'R'.$booking['booking']->id ?></span></h3>
                            <hr />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <address>
                                            <h3><?= $this->config->item('hotel_name') ?></h3>
                                            <p class="text-muted m-l-5">
                                                <?= $this->config->item('address1') ?><br/>
                                                <?= $this->config->item('address2') ?><br/>
                                                <?= $this->config->item('landmark') ?>
                                            </p>
                                        </address>
                                    </div>
                                    <div class="pull-right text-right">
                                        <address>
                                            <p class="addr-font-h3">To,</p>
                                            <p class="font-bold addr-font-h4"><?= $booking['guest']->guest_name ?></p>
                                            <p class="text-muted m-l-30">
                                                <?= $booking['guest']->address ?><br/>
                                                <?= $booking['guest']->phone_number ?><br/>
                                                <?= $booking['guest']->email_address ?><br/>
                                                <table>
                                                    <tr>
                                                        <td><b>Check In Time: </b></td><td> <?= date('d/m/Y', strtotime($booking['booking']->check_in)) . ' ' . $booking['booking']->checkin_time ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Check Out Time: </b></td><td> <?= date('d/m/Y', strtotime($booking['booking']->check_out)) . ' ' . $booking['booking']->checkout_time ?></td>
                                                    </tr>
                                                </table>
                                            </p>
                                            <p class="m-t-30">
                                                <b>Invoice Date :</b> <i class="fa fa-calendar"></i> <?= DateTime::createFromFormat('Y-m-d',$booking['booking']->invoice_date)->format('d/m/Y') ?>
                                            </p>
                                        </address>
                                    </div>
                                </div>
                                <div class="col-md-12" id="accommodation1">
                                    <h4>Accommodation <span title="Show in print" id="accomodation_visible" style="cursor:pointer;" onclick="toggleAccommodation()" class="no-print fa fa-eye"></span> </h4>
                                </div>
                                <div id="accommodation2" class="col-md-12">
                                    <div class="table-responsive m-t-40">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Room No</th>
                                                <th class="text-center">HSN</th>
                                                <th class="text-center">Rent</th>
                                                <th class="text-center">Charges</th>
                                                <th class="text-center">No Of Days</th>
                                                <th class="text-center">GST</th>
                                                <th class="text-right">Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $total_cost = 0; $sub_total=0; $total_tax = 0; $count=1; foreach ($booking['rooms'] as $room) { ?>
                                                <tr>
                                                    <td class="text-center"><?= $count++ ?></td>
                                                    <td class="text-center"><?= $room->room_number ?></td>
                                                    <td class="text-center"><?= $room->HSN ?></td>
                                                    <td class="text-center"><?= $room->room_name ?></td>
                                                    <td class="text-center">Rs.<?= $room->price ?></td>
                                                    <?php $day_count = date_diff(date_create($booking['booking']->check_in),date_create($booking['booking']->check_out))->format("%d"); ?>
                                                    <td class="text-center"><?= $day_count ?></td>
                                                    <?php $tax = $room->price*$day_count*$this->config->item('gst')/100; $total_tax+=$tax; ?>
                                                    <td class="text-center">Rs.<?= $tax ?></td>
                                                    <?php $gst_price = $room->price*$day_count + $tax; $sub_total+=$room->price*$day_count; ?>
                                                    <?php $gst_price = $room->price*$day_count*(100+$this->config->item('gst'))/100; ?>
                                                    <td class="text-right">Rs.<?= $gst_price ?></td>
                                                    <?php $total_cost+=$gst_price; ?>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="pull-right m-t-30 text-right">
                                        <?php
                                            $grand_total = round($total_cost);
                                            $round_off = $grand_total - $total_cost;
                                        ?>
                                        <p>Sub - Total amount: Rs.<?= $sub_total ?></p>
                                        <p>CGST (<?= $this->config->item('gst')/2 ?>%) : Rs.<?= $total_tax/2 ?> </p>
                                        <p>SGST (<?= $this->config->item('gst')/2 ?>%) : Rs.<?= $total_tax/2 ?> </p>
                                        <hr />
                                        <h4><b>Total :</b> Rs.<?= $total_cost ?></h4>
                                        <h5><b>Round off :</b> Rs.<?= round($round_off,2) ?></h5>
                                        <h3><b>Grand Total :</b> Rs.<?= $grand_total ?></h3>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr />
                                    <div class="text-right">
                                        <button class="btn btn-danger no-print" type="submit"> Proceed to payment </button>
                                        <button onclick="javascript:window.print();" class="btn btn-default btn-outline no-print" type="button"> <span><i class="fa fa-print"></i> Print</span> </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end page content -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <div class="page-footer">
        <div class="page-footer-inner"> <?= date('Y') ?> &copy; Developed by
            <a href="#" target="_top" class="makerCss">Algosoft</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
    <!-- end footer -->
</div>
<!-- start js include path -->
<script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/popper/popper.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
<script src="<?= base_url() ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap -->
<script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Common js-->
<script src="<?= base_url() ?>/assets/js/app.js"></script>
<script src="<?= base_url() ?>/assets/js/layout.js"></script>
<script src="<?= base_url() ?>/assets/js/theme-color.js"></script>
<!-- Material -->
<script src="<?= base_url() ?>/assets/plugins/material/material.min.js"></script>
<!-- animation -->
<script src="<?= base_url() ?>/assets/js/pages/ui/animations.js"></script>

<!-- Sweet Alert -->
<script src="<?= base_url() ?>/assets/plugins/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>/assets/js/pages/sweet-alert/sweet-alert-data.js"></script>
<script src="<?= base_url() ?>assets/plugins/material/material.min.js"></script>
<script src="<?= base_url() ?>assets/js/pages/material_select/getmdl-select.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="<?= base_url() ?>assets/plugins/material-datetimepicker/datetimepicker.js"></script>
<!-- end js include path -->
<script>
    var aVisible = true;
    var eVisible = true;
    function toggleAccommodation(){
        if(aVisible) {
            $("#accomodation_visible").removeClass("fa fa-eye");
            $("#accomodation_visible").addClass("fa fa-eye-slash");
            aVisible = false;
            $("#accommodation1").addClass("no-print");
            $("#accommodation2").addClass("no-print");
        }else{
            $("#accomodation_visible").removeClass("fa fa-eye-slash");
            $("#accomodation_visible").addClass("fa fa-eye");
            aVisible = true;
            $("#accommodation1").removeClass("no-print");
            $("#accommodation2").removeClass("no-print");
        }
    }
</script>
</body>
</html>